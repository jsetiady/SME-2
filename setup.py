# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='Gojek - Social Media Emotion',
    version='0.0.1',
    description='Gojek - Social Media Emotion',
    url='https://gitlab.com/jsetiady/SME-2',
)
