from flask import Flask, request
from flask_restful import Resource, Api
#from flash_restplus import Resource, Api
from flask_cors import CORS
from json import dumps
import os
import scrapper.generate_statistics as stat
import sentiment.up_dash as update_dashboard
import scrapper.update_sentiment as update_sentiment
from flask_restful import reqparse

app = Flask(__name__)
api = Api(app)
CORS(app)

class daily_stats(Resource):
    def get(self, day):
        return stat.daily_stats(day)

class locate_sentiment(Resource):
    def get(self, sentiment_string, first_day, last_day):
        result = stat.locate_sentiment(sentiment_string, first_day, last_day)
        return result

class count_sentiment(Resource):
    def get(self, first_day, last_day):
        result = stat.count_sentiment(first_day, last_day)
        return list(result)

class Top_User(Resource):
    def get(self, first_day, last_day):
        result = stat.show_most_frequented_users(first_day, last_day)
        return result

class Main(Resource):
	def get(self):
		result = {'status': "success"}
		return result

class Important_Tweets(Resource):
    def get(self, first_day, last_day):
        return stat.get_most_important_tweets(first_day, last_day)

class New_Followers(Resource):
    def get(self, day):
        return stat.get_new_followers(day)

class Churn_Followers(Resource):
    def get(self, day):
        return stat.get_churn_followers(day)

class Daily_Followers(Resource):
    def get(self, day):
        return stat.get_daily_followers(day)

class Sentiment_Demo(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('text', required=True, help="Text cannot be blank!")
        args = parser.parse_args()
        text = args["text"]
        print text
        # create tsv
        with open("Result/results.tsv", "w") as record_file:
            record_file.write("ID\tText\tSentiment\n")
            record_file.write("%s\t%s\t%s\n" % ("1", text, "9"))

        # call up_dash
        update_dashboard.up_dash("Result/results.tsv", "Result/sentiment_result.tsv")

        # get sentiment from tsv
        data = update_sentiment.tsv_to_dataframe("Result/sentiment_result.tsv")
        sentiment = str(data.at[0,'Sentiment'])

        json = {
            "sentiment" : sentiment
        }

        return json

api.add_resource(daily_stats, '/stats/daily/<string:day>')
api.add_resource(locate_sentiment, '/sentiment/locate/<string:sentiment_string>/<string:first_day>/<string:last_day>')
api.add_resource(Top_User, '/followers/top/<string:first_day>/<string:last_day>')
api.add_resource(Important_Tweets, '/tweets/important/<string:first_day>/<string:last_day>')
api.add_resource(count_sentiment, '/sentiment/count/<string:first_day>/<string:last_day>')
api.add_resource(New_Followers, '/followers/new/<string:day>')
api.add_resource(Churn_Followers, '/followers/churn/<string:day>')
api.add_resource(Daily_Followers, '/followers/<string:day>')
api.add_resource(Sentiment_Demo, '/sentiment/demo')
api.add_resource(Main, '/')

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host='0.0.0.0', port=port, threaded=True)
	#app.run()
