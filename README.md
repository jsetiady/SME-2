# Sentiment Analysis Update (21/7/17)
Our benchmark is a Bag of Words model with a Random Forest classifier. 

    Random Forest 10-fold Accuracy: 0.66 (+/- 0.09)
    Random Forest Accuracy: 0.70
    Random Forest F1 score: 0.70
    Random Forest Recall: 0.70
    Random Forest Precision: 0.70
    Random Forest clasification report:
        
                  precision   recall   f1-score   support
                  
             -1      0.67      0.76      0.71       383
              0      0.76      0.71      0.73       488
              1      0.57      0.43      0.49        91
        
        avg/total    0.70      0.70      0.70       962
    
    Random Forest confussion matrix: 
    (86 posts with a real sentiment of -1 were mistakenly predicted to have a sentiment of 0)
    
	    -1     0    1
    -1: 291   86   6
     0: 116   348  24
     1: 26    26   39  

![BoW Accuracy vs. Training Data Size](https://docs.google.com/spreadsheets/d/e/2PACX-1vRm6IJ40WQ_w1hBajRqcmAfLngJv8hIOIypXh7Z3Eiuz5qR2LOsAblaVOjVa7zdQzEl9DGRF2qQq8-8/pubchart?oid=2147377726&format=image)

The only model/classifier combination to convincingly beat Bag of Words/Random Forest was FastText's supervised training algorithm using FastText's pre-trained Indonesian word vector model which was trained on Wikipedia texts. 

     Accuracy: 0.72
     F1 score: 0.72
     Recall: 0.72
     Precision: 0.72
     clasification report:
     
                  precision    recall  f1-score   support
    
             -1       0.70      0.78      0.73       383
              0       0.78      0.72      0.74       488
              1       0.55      0.52      0.53        91
    
    avg / total       0.72      0.72      0.72       962
    
     Confusion Matrix: 
     
         -1   0    1
    -1: [297  74  12]
     1: [112 349  27]
     0: [ 17  27  47] 

![FastText Accuracy vs. Data Size ](https://docs.google.com/spreadsheets/d/e/2PACX-1vRm6IJ40WQ_w1hBajRqcmAfLngJv8hIOIypXh7Z3Eiuz5qR2LOsAblaVOjVa7zdQzEl9DGRF2qQq8-8/pubchart?oid=1913875442&format=image)

Our next steps should be:
1. First of all, see if linear improvement continues with larger datasets for FastText supervised algorithm. This is [the paper that explains the FastText algorithm](https://research.fb.com/wp-content/uploads/2016/07/eacl2017.pdf?). It is much faster than "models based on deep neural nets" and its accuracy is comparable/on par (read the paper for more detailed comparisons). 
2. Try to build Word2Vec and FastText models with very large unsupervised datasets (i.e. at least on the order of magnitude of 10,000). 
3. We could try to learn about and experiment with "models based on deep neural nets." 