# Social Media Sentiment Analysis Project 
The goal of this project is to deliver a dashboard that will (amongst other things) show sentiment towards Go-Jek on social media over any given time period. In order to do this, we must first build a model to classify posts into 1 of 3 categories: negative (-1), neutral (0), or positive (1). 

Our first iteration approach consists of 3 steps:
1. Pre-process data by removing stopwords (words with little meaning relevant to sentiment), hashtags and usernames, and stemming (replacing playing, played, play with play). 
2. Build a vocabulary and translate that vocabulary to a vector space representation using the Doc2Vec algorithm. Doc2Vec is based on the *Paragrah Vector* algorithm presented in [Distributed Representations of Sentences and Documents (Le & Mikolov)](https://cs.stanford.edu/~quocle/paragraph_vector.pdf). This step does not required labeled data. 
3. Training classifiers on data labeled with sentiment. So far, we have tried Logistic Regression, SVM, Random Forest, Naive Bayes, and Multi Layer Perceptron. 

### Our Code
Our code is in the 'sentiment' folder. 
- word2vec-sentiments.py: 
This is  [Linan Qiu's code](http://linanqiu.github.io/2015/10/07/word2vec-sentiment/) which forms the foundation of our code. 
- 1gramWord2Vec.py
This is our adapation of the above code, which builds a single word vocab. 
- 2gramWord2Vec.py
This is the version of the code that builds a vocab with 2 word phrases in addition to single words. 

### How accuracy depends on training data size (cross-validation)

Our training data consists of 1962 Facebook comments labeled with sentiment. All 1962 comments are used for vectorization (step 2). Half of the data is used for classification training (step 3) and half is for classification training. 

*Note that +/- steadily decreases as training data size increases*

![1-gram accurcy vs. data size](https://docs.google.com/spreadsheets/d/1mjG3mcqfY0PTK32eC_cruyUlRP3WN2wMytUiVlZk4U0/pubchart?oid=1121110733&format=image)

![2-gram accuracy vs. data size](https://docs.google.com/spreadsheets/d/1mjG3mcqfY0PTK32eC_cruyUlRP3WN2wMytUiVlZk4U0/pubchart?oid=1344666067&format=image)

### Precision & Recall for various classifiers on full training data set for 2-grams

*Note: This is how to read a confusion matrix*

![confusion matrix](http://scikit-learn.org/stable/_images/sphx_glr_plot_confusion_matrix_002.png)

**Linear Regression:**
*Classification Report:*

           precision    recall    f1-score   support
    -1.0      0.61        0.59      0.60       383
    0.0       0.64        0.77      0.70       488
    1.0       0.00        0.00      0.00        91
    avg/total 0.57        0.63      0.59       962

*Confusion matrix:*
(true label down the left side, predicted label across the top)

        -1   0   1
    -1: 227 149  7
     0: 113 375  0
     1: 33  58   0

**Random Forest** 
*Classification report:*

            precision   recall  f1-score   support
    -1.0      0.60       0.69      0.64       383
    0.0       0.69       0.71      0.70       488
    1.0       0.71       0.11      0.19        91
    avg/total 0.65       0.65      0.63       962

*Confusion matrix:*
(true label down the left side, predicted label across the top)

        -1   0   1
    -1: 265 118  0
     0: 138 346  4
     1: 42  39   10

**SVM** 
*Classification report:*

            precision   recall    f1-score   support
    -1.0      0.61        0.34      0.44       383
    0.0       0.57        0.88      0.69       488
    1.0       0.00        0.00      0.00        91
    avg/total 0.53        0.58      0.53       962
*Confusion matrix:*
(true label down the left side, predicted label across the top)

        -1   0   1
    -1: 131 252  0
     0: 61  427  0
     1: 23  68   0
**Multi Layer Perceptron** 
*Classification report:*

            precision   recall   f1-score   support
    -1.0      0.60       0.62      0.61       383
    0.0       0.65       0.75      0.70       488
    1.0       0.00       0.00      0.00        91
    avg/total 0.57       0.63      0.60       962
*Confusion matrix:*
(true label down the left side, predicted label across the top)

        -1   0   1
    -1: 237 146  0
     0: 121 367  0
     1: 39  52   0

**Naive Bayes**
*Classification report:*

             precision    recall   f1-score   support
    -1.0       0.65        0.22      0.33       383
    0.0        0.55        0.05      0.10       488
    1.0        0.10        0.86      0.18        91
    avg/total  0.55        0.20      0.20       962

*Confusion matrix:*
(true label down the left side, predicted label across the top)    
    
        -1   0   1
    -1: 86  17  280
     0: 38  26  424
     1: 9   4   78

### Precision & Recall for various classifiers on full training data set for 1-grams

**Linear Regression:**
*Classification Report:*

               precision   recall   f1-score   support
       -1.0       0.58      0.48      0.53       383
        0.0       0.61      0.80      0.69       488
        1.0       0.00      0.00      0.00        91
    avg/total     0.54      0.60      0.56       962

*Confusion Matrix:*

         -1   0   1
    -1:  185 198  0
     0:  96  392  0
     1:  36  55   0
**Random Forest**
*Classification Report:*

             precision    recall  f1-score   support
    -1.0       0.57      0.62      0.59       383
    0.0        0.65      0.70      0.67       488
    1.0        0.31      0.04      0.08        91
    avg/total  0.58      0.61      0.58       962
*Confusion matrix:*

         -1   0   1
    -1:  239 140  4
     0:  142 341  5
     1:  40  47   4

**Naive Bayes**
*Classification Report:*

              precision    recall  f1-score   support
        -1.0      0.58      0.15      0.23       383
        0.0       0.50      0.10      0.17       488
        1.0       0.10      0.81      0.17        91
    avg/total     0.50      0.19      0.20       962
*Confusion matrix:*

         -1   0   1
    -1:  56  42  285
     0:  31  50  407
     1:  9   8   74

**SVM**
*Classification Report:*

             precision   recall   f1-score   support
       -1.0     0.61      0.13      0.21       383
        0.0     0.53      0.95      0.68       488
        1.0     0.00      0.00      0.00        91
    avg/total   0.51      0.53      0.43       962
*Confusion Matrix:*

         -1   0   1
    -1:  48  335  0
     0:  22  466  0
     1:  9   82   0

**Multi Layer Perceptron**
*Classification Report:*

                precision    recall  f1-score   support
       -1.0       0.61      0.51      0.55       383
        0.0       0.62      0.82      0.70       488
        1.0       0.00      0.00      0.00        91
    avg/total     0.56      0.62      0.58       962
*Confusion Matrix:*

         -1   0   1
    -1:  194 189  0
     0:  89  399  0
     1:  35  56   0
