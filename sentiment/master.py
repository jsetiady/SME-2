import numpy as np
import pandas as pd

from preprocessing.crowdcrafting import labeled_only_df
from preprocessing.Readers import tsv_to_df, tsvs_to_df, json_to_df, shuffle
from preprocessing.Tokenizers import bigram_df, process_df, process_sentence
from build_models.build_models import vectorize_sentences, build_sentence_vector

# word vector models
from gensim.models import Word2Vec
from gensim.models.wrappers import FastText
from gensim.models.keyedvectors import KeyedVectors

#classifiers
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score, precision_score, f1_score, \
     confusion_matrix, recall_score

"""
# graph vs datasize
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
plotly.tools.set_credentials_file(username='rheza', api_key='6RMGKMc8L2StK1xUCEPv')
"""

# LSTMs
from Wilson_LSTM import Wilson_LSTM
from Philippe_LSTM import Philippe_LSTM

class word_vectors:
    def __init__(self, model_type):
        self.type = model_type

    def train(self, tokenized_sentences, model_size):
        if self.type == 'word2vec':
            self.model = Word2Vec(tokenized_sentences,
                                       size=model_size,
                                       workers=2,
                                       iter=5,
                                       window=5,
                                       sg=1,
                                       hs=1,
                                       min_count=5)
        elif self.type == 'fasttext':
            FastText(sentences=tokenized_sentences, size=model_size)
    
    def train_df(self, df, model_size, bigram=True):
        pdf = process_df(df)
        
        if bigram==True:
            pdf = bigram_df(pdf)
        
        train_sentences = pdf["Text"]
        self.train(train_sentences, model_size)

    def load(self, model_file):
        # use wv with w2v to access word vector instances
        if self.type=='word2vec':
            self.model = Word2Vec.load(model_file)
        elif self.type=='keyedvectors':
            self.model = KeyedVectors.load(model_file)
        elif self.type=='fasttext':
            self.model = FastText.load_binary_data(model_file)
        else:
            print "Please input a valid model_type"
        
    def save(self, model_file):
        # if isinstance(self.model, Word2Vec):
        self.model.save(model_file)

class sentences_df:
    def __init__(self, files):
        if isinstance(files, str):
            self.raw_df = tsv_to_df(files)
        elif isinstance(files, list):
            self.raw_df = tsvs_to_df(files)
        else:
            print "Please enter a filepath or list of filepaths"

    def make_tokenized_df(self, bigram=True, remove_punctuation=True, 
                          fix_typos=False):
        self.tokenized_df = process_df(self.raw_df, remove_punctuation, 
                                       fix_typos)
        if bigram==True:
            self.tokenized_df = bigram_df(self.tokenized_df)

    def split_train_test(self, train_num, test_num):
        if train_num+test_num > len(self.tokenized_df):
            print "WARNING: Your train_num + test_num exceeds total data num"
        
        self.tokenized_df = shuffle(self.tokenized_df)
        
        self.train_df = self.tokenized_df.head(train_num).reset_index(drop=True)
        self.test_df = self.tokenized_df.tail(test_num).reset_index(drop=True)

    def vectorize_to_array(self, wv_model, wvm_size):
        self.train_array_x = vectorize_sentences(self.train_df["Text"],
                                               wv_model, wvm_size)
        self.test_array_x = vectorize_sentences(self.test_df["Text"],
                                              wv_model, wvm_size)
    
    def labels_to_array(self):
        self.train_array_y = self.train_df["Sentiment"].as_matrix()
        self.test_array_y = self.test_df["Sentiment"].as_matrix()

    def prep_supervised(self, wv_model, wvm_size, train_num, test_num):
        self.make_tokenized_df()
        self.split_train_test(train_num, test_num)
        self.vectorize_to_array(wv_model, wvm_size)
        self.labels_to_array()


class sentences2:
    def import_tsvs(self, unlabeled_files=None, labeled_files=None):
        if unlabeled_files:
            self.unlabeled = tsvs_to_df(unlabeled_files)
        if labeled_files:
            self.labeled = tsvs_to_df(labeled_files)
    
    def tokenize(self, bigram=False, remove_punctuation=True, fix_typos=False):
        self.unlabeled = process_df(self.unlabeled, remove_punctuation, 
                                    fix_typos).reset_index()
            
        self.labeled = process_df(labeled_only_df(self.labeled), 
                                  remove_punctuation, fix_typos).reset_index()
        if bigram==True: 
            self.unlabeled = bigram_df(self.unlabeled)
            self.labeled = bigram_df(self.labeled)

    def split_labeled(self, train_num, test_num):
        self.labeled = shuffle(self.labeled)
        self.labeled_train = self.labeled.head(train_num).reset_index()
        self.labeled_test = self.labeled.tail(test_num).reset_index()

    def make_unsupervised_train(self, train_num):
        all_train_sentences = pd.concat([self.unlabeled, self.labeled_train], 
                                        ignore_index=True)
        all_train_sentences = shuffle(all_train_sentences)
        self.unsupervised_train = all_train_sentences.head(train_num)

    def vectorize_to_array(self, model, model_size):
        self.train_array = vectorize_sentences(self.labeled_train["Text"],
                                               model, model_size)
        self.test_array = vectorize_sentences(self.labeled_test["Text"],
                                              model, model_size)

    def labels_to_array(self):
        self.train_array_labels = self.labeled_train["Sentiment"].as_matrix()
        self.test_array_labels = self.labeled_test["Sentiment"].as_matrix()

    def default_process(self, unlabeled_files, labeled_files, 
                        model, model_size, sup_train_num, 
                        test_num, unsup_train_num):
        self.import_tsvs(unlabeled_files, labeled_files)
        self.tokenize(bigram=True)
        self.split_labeled(sup_train_num, test_num)
        self.make_unsupervised_train(unsup_train_num)
        self.vectorize_to_array(model, model_size)
        self.labels_to_array()


class classifier:
    def __init__(self, clf_name, wv_model, wv_model_size):
        self.wvm = wv_model
        self.wvm_size = wv_model_size
        self.clf_name = clf_name

        if clf_name=='maxent':
            self.clf = LogisticRegression()
        elif clf_name=='forest':
            self.clf = RandomForestClassifier()
        elif clf_name=='mlp':
            self.clf = MLPClassifier()
        elif clf_name=='phil_lstm':
            self.clf = Philippe_LSTM(self.wvm, self.wvm_size)
        elif clf_name=='wil_lstm':
            self.clf = Wilson_LSTM(self.wvm, self.wvm_size)
        else:
            print "Please enter a valid classifier name"

    def train(self, df, bigram=True,
              timesteps=20, split_fraction=0.9, epochs=25, batch_size=10, 
                        avg_timestep = True,
                        shuffle = True,
                        reg = 0.01,
                        rdropout = 0.03,
                        dropout = 0.5,
                        learning_rate = 0.01,
                        clipvalue = 0.1):
        
        pdf = process_df(df)
       
        if bigram==True:
            pdf = bigram_df(pdf)

        if self.clf_name=='wil_lstm':
            self.clf.trainNewModel(timesteps = timesteps, 
                                   split_fraction = split_fraction,
                                   epochs = epochs,
                                   batchSize = batch_size,
                                   sentences = pdf,
                                   isShuffle = shuffle)
        elif self.clf_name=='phil_lstm':
            self.clf.trainNewModel(sentences = pdf, 
                                   split_fraction = split_fraction, 
                                   epochs = epochs,
                                   avg_timestep = avg_timestep,
                                   shuffle = shuffle,
                                   reg = reg,
                                   rdropout = rdropout,
                                   dropout = dropout,
                                   learning_rate = learning_rate,
                                   clipvalue = clipvalue)
        else:
            train_array = vectorize_sentences(df["Text"],
                                            self.wvm,
                                            self.wvm_size)
            train_labels = df["Sentiment"].as_matrix()
            self.clf.fit(train_array,train_labels)

    def retrain(self, df, split_fraction, epochs=10):
        if clf_name=='wil_lstm':
            print "Sorry, Wil's LSTM doesn't support retraining yet."
        elif clf_name=='phil_lstm':
            self.clf.fitNewData(sentences=df, testSplit_frac=split_fraction, 
                                epochs = epochs)
        else:
            train_array = vectorize_sentences(df["Text"],
                                            self.wvm,
                                            self.wvm_size)
            train_labels = df["Sentiment"].as_matrix()
            self.clf.fit(train_array,train_labels)
    
    def classify(self, string):
        if self.clf_name=='wil_lstm':
            return self.clf.predictString(string)
        elif self.clf_name=='phil_lstm':
            return self.clf.predictString(string)
        else:
            tokenized_sentence = process_sentence(string)
            vector = build_sentence_vector(self.wvm, tokenized_sentence, 
                                            self.wvm_size)
            return self.clf.predict(vector)
    
    def identify_speech_type(self, string):
        if '?' in string:
            return 'Question'
        else:
            return 'Statement'

    def identify_products(self, string):
        products_mentioned = []
        
        keywords = {'gojek': ['gojek', 'go-jek', '#gojek', 'go jek'], 
                    'goride': ['goride', 'go-ride', '#goride', 'go ride'], 
                    'gocar': ["gocar", "go-car", "#gocar", 'go car'], 
                    'gosend':  ["gosend", "go-send", "#gosend", 'go send'],
                    'gomart': ['gomart', 'go-mart', '#gomart', 'go mart'],
                    'gomassage': ['gomassage', 'go-massage', '#gomassage', 'go massage'],
                    'goclean': ['goclean', 'go-clean', '#goclean', 'go clean'],
                    'goglam': ['goglam', 'go-glam', '#goglam', 'go glam']}

        for product in keywords.keys():
            if any(keyword in string for keyword in keywords[product]):
                products_mentioned.append(product)

        return products_mentioned
    
    def classify_df(self, df, text=False, 
                    speech_type=False, product=False):

        dicts = []
        for i in xrange(0,len(df)):
            text = str(df['Text'][i])
            y = int(self.classify(text))

            dict = {"ID" : df["ID"][i], "Sentiment" : y}

            if speech_type==True:
                dict['Speech_Type'] = self.identify_speech_type(text)

            if product==True:
                dict['Product'] = self.identify_products(text)
            
            if text==True:
                dict['Text'] = text
            
            dicts.append(dict)

        return pd.DataFrame(dicts) 

    def classify_tsv(self, in_file, out_file, 
                     text=False, speech_type=False, product=False):
        in_df = tsv_to_df(in_file)
        out_df = self.classify_df(in_df, text=text, speech_type=speech_type, 
                                  product=product)
        out_df.to_csv(out_file, sep='\t')

    def classify_json(self, in_file, out_file, 
                      text=False, speech_type=False, product=False):
        in_df = json_to_df(in_file)
        out_df = self.classify_df(in_df, text=text, speech_type=speech_type,
                                  product=product)
        out_df.to_json()
    
    def save(self, fname):
        if self.clf_name=='wil_lstm':
            self.clf.save(fname)
        elif self.clf_name=='phil_lstm':
            self.clf.save(fname)
        else:
            joblib.dump(self.clf, fname)

    def load(self, fname):
        if self.clf_name=='wil_lstm':
            self.clf.load(fname)
        elif self.clf_name=='phil_lstm':
            self.clf.load(fname)
        else:
            self.clf = joblib.load(fname)

    def score(self, test_df, accuracy=True, precision=True, recall=True, f1=True, 
              confusion=True):
        self.metrics = {}
        classified_df = self.classify_df(test_df)
        y_true = test_df["Sentiment"].as_matrix()
        y_pred = classified_df["Sentiment"].as_matrix()

        print "\nMETRICS FOR "+str(self.clf_name)+":\n"
        
        if accuracy==True:
            self.metrics['accuracy'] = accuracy_score(y_true = y_true, 
                                                 y_pred = y_pred)
            print "Accuracy: "+str(self.metrics['accuracy'])

        if precision==True:
            self.metrics['precision'] = precision_score(y_true = y_true, 
                                                        y_pred = y_pred, 
                                                        average = 'micro')
            print "\nPrecision: "+str(self.metrics['precision'])

        if recall==True:
            self.metrics['recall'] = recall_score(y_true = y_true, 
                                                    y_pred = y_pred, 
                                                  average = 'micro')
            print "\nRecall: "+str(self.metrics['recall'])

        if f1==True:
            self.metrics['f1'] = f1_score(y_true = y_true, 
                                     y_pred = y_pred,
                                     average = 'micro')
            print "\nF1: "+str(self.metrics['f1'])

        if confusion==True:
            self.metrics['confusion'] = confusion_matrix(y_true = y_true,
                                                    y_pred = y_pred)
            print "\nConfusion: \n"+ str(self.metrics['confusion'])

        return self.metrics



