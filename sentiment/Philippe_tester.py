from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
from keras.models import Sequential
from keras.layers import *
import matplotlib.pyplot as plt
from keras.optimizers import *
import numpy as np
import pandas as pd
from Attention import *
from crowdcrafting import tasks_to_df, labeled_only_df
import math
from keras.callbacks import *
from Philippe_LSTM import Philippe_LSTM

w2vmodel = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300
crowd_df = labeled_only_df(tasks_to_df('./datasets/crowdcrafting/tasks_1.json',
                            './datasets/crowdcrafting/runs_1.json'))
train_df = pd.concat([ tsv_to_dataframe('./datasets/facebook_part1.tsv'),
                      rpw_to_df('./datasets/fb4.tsv')],
                     ignore_index=True)
train_df = pd.concat([train_df, tsv_to_dataframe('./datasets/facebook_part2.tsv')])
train_df = labeled_only_df(train_df).reset_index()
train_sentences = bigram_df(process_df(train_df))

lstm = Philippe_LSTM(w2vmodel, 300)
#lstm.trainNewModel(timesteps=37, epochs=1, batch_size=80,
#                   split_fraction=0.99, sentences=train_sentences)
#lstm.save('./lstm1')
lstm.load('./lstm1')
lstm.predictString("gue sangat suka go")
