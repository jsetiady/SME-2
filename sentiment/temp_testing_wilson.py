from Wilson_LSTM import Wilson_LSTM
import tensorflow as tf
from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
from preprocessing.Tokenizers import process_df
from preprocessing.Tokenizers import bigram_df
from preprocessing.Tokenizers import df_to_tokenized_sentences
from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences

from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np

from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
from collections import defaultdict
import math
import matplotlib.pyplot as plt
from crowdcrafting import tasks_to_df, labeled_only_df

model=0#this is to be trained.
max_sequence=20#default.
num_dimensions=300

embedding_model=0


train_file = './datasets/facebook_part1.tsv'
test_file  = './datasets/facebook_part2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)

log.info("Tokenizing combined sentences")
crowd_df = labeled_only_df(tasks_to_df('./datasets/crowdcrafting/tasks_1.json',
                            './datasets/crowdcrafting/runs_1.json'))

train_df = pd.concat([crowd_df, tsv_to_dataframe(train_file), rpw_to_df('./datasets/fb4.tsv')], ignore_index=True)
train_df=labeled_only_df(train_df)
train_df=train_df.reset_index(drop=True)

test_df = tsv_to_dataframe(test_file)
test_df=labeled_only_df(test_df).reset_index()

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

#concatenate both train and test sentences for simplicity.
train_sentences= pd.concat([train_sentences,test_sentences],ignore_index=True)
log.info("Building Word2Vec")
# https://radimrehurek.com/gensim/models/word2vec.html
w2vmodel = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300

# ---------------------------- HOW TO USE MODEL ------------------------------ #
lstm = Wilson_LSTM(w2vmodel, 300)
lstm.trainNewModel(timesteps=20, split_fraction=0.85, epochs=1, batchSize=10, 
                   sentences=train_sentences)
lstm.save('new_wilson_lstm')
lstm.load('new_wilson_lstm')

"""
# ----------------------------HOW TO USE MY (OLD) MODEL.------------------------------------------#
lstm = Wilson_LSTM(w2vmodel,20,300,train_sentences,lstm_model_name="willyz_lstm2")
lstm.trainNewModel(timesteps=20,split_fraction=0.85,epochs=25,batchSize=10)
lstm.save()
lstm.load()
"""
