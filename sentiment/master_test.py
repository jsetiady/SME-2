from master import word_vectors, sentences_df, classifier
from testing.Logger import graph_vs_datasize

"""
model_size=100
wvm = word_vectors('keyedvectors')
wvm.load('./models/lstm.bin')
clf = classifier('maxent', wvm.model, 300)
"""

def train_and_save():
    labeled = sentences_df(['./datasets/facebook_part1.tsv', 
                            './datasets/facebook_part2.tsv',
                            './datasets/twitter.tsv'])

    all = sentences_df(['./datasets/bahasa_tweets(12k).tsv', 
                        './datasets/twitter.tsv', 
                        './datasets/fb_3(45k).tsv', 
                        './datasets/tweets_large(18k).tsv',
                        './datasets/facebook_part1.tsv', 
f                       './datasets/facebook_part2.tsv'])
 
    wvm = word_vectors('word2vec')
    wvm.train_df(all.raw_df, 300)
    
    clf = classifier('phil_lstm', wvm.model, 300)
    clf.train(labeled.raw_df)
    
    wvm.save('./models/w2v_app.bin')
    clf.save('./models/phil_lstm')

    test = sentences_df('./datasets/facebook_part1.tsv')
    clf.score(test.raw_df)

    

def test5():
    wvm = word_vectors('keyedvectors')
    wvm.load('./models/lstm.bin')
    
    s = sentences_df('./datasets/facebook_part2.tsv')

    lstm1 = classifier('phil_lstm', wvm.model, 300)

    lstm1.load('./models/ignore/lstm1')

    return lstm1.classify_df(s.raw_df)

def test4():
    wvm = word_vectors()
    wvm.load('keyedvectors', './models/lstm.bin')
    
    s = sentences_df('./datasets/facebook_part2.tsv')


    lstm1 = classifier('phil_lstm', wvm.model, 300)
    # lstm2 = classifier('wil_lstm', wvm.model, 300)

    lstm1.load('./models/ignore/lstm1')
    # lstm2.load('./models/ignore/lstm2')

    lstm1_metrics = lstm1.score(s.raw_df)
    # lstm2_metrics = lstm2.score(s.raw_df)


def test3():
    wvm = word_vectors()
    wvm.load('keyedvectors', './models/lstm.bin')

    # lstm1 = classifier('phil_lstm', wvm.model, 300)
    lstm2 = classifier('wil_lstm', wvm.model, 300)
    maxent = classifier('maxent', wvm.model, 300)

    s = sentences('./datasets/facebook_part1.tsv')
    s.prep_supervised(wvm.model, 300, 800, 100)
    
    # lstm1.train(s.raw_df, epochs=1)
    # lstm2.train(s.raw_df, epochs=1)
    # maxent.train(s.raw_df)

    """
    lstm1.classify_tsv('./datasets/facebook_part2.tsv', 
                       './datasets/lstm1.tsv')
    
    lstm2.classify_tsv('./datasets/facebook_part2.tsv', 
                       './datasets/lstm2.tsv')
    
    maxent.classify_tsv('./datasets/facebook_part2.tsv',
                        './datasets/maxent.tsv')
    """

    # lstm1.save('./models/lstm1')
    # lstm2.save('./lstm2')
    # maxent.save('./models/maxent.pkl')

    # lstm1.load('./models/lstm1')
    lstm2.load('./models/lstm2')
    # maxent.load('./models/maxent.pkl')
    """
    lstm1.classify_tsv('./datasets/facebook_part2.tsv', 
                       './datasets/lstm1_load.tsv')
    """
    lstm2.classify_tsv('./datasets/facebook_part2.tsv', 
                       './datasets/lstm2_load.tsv')
    """
    maxent.classify_tsv('./datasets/facebook_part2.tsv',
                        './datasets/maxent_load.tsv')
                        """


def test2():
    wv = word_vectors()

    wv.load('keyedvectors','./models/lstm.bin')

    sens = sentences2()
    sens.default_process(unlabeled_files=['./datasets/tweets_large(18k).tsv',
                                       './datasets/fb_3(45k).tsv', 
                                       './datasets/fb_large(14k).tsv'],
                      labeled_files=['./datasets/facebook_part1.tsv',
                                      './datasets/facebook_part2.tsv'],
                     model=wv.model, model_size=300, sup_train_num=1200,
                     test_num=240, unsup_train_num=50000)

    clf = classifier(wv.model,300)

    
    clf.maxent.fit(sens.train_array, sens.train_array_labels)
    
    graph_vs_datasize('master test', clf.maxent, 10, 'f1',
                    sens.train_array, sens.train_array_labels,
                    sens.test_array, sens.test_array_labels)


def test1():
    sens = sentences2()

    sens.import_tsvs(labeled_files=['./datasets/facebook_part1.tsv',
                                    './datasets/facebook_part2.tsv'])

    sens.tokenize(bigram=True)
    
    sens.split_labeled(train_num=1000, test_num=900)

    sens.make_unsupervised_train(50000)

    wv.train('word2vec', sens.unsupervised_train["Text"], model_size)

    sens.vectorize_to_array(wv.model, model_size)

    sens.labels_to_array()

    clf.maxent.fit(sens.train_array, sens.train_array_labels)
    
    graph_vs_datasize('master test', clf.maxent, 10, 'f1',
                    sens.train_array, sens.train_array_labels,
                    sens.test_array, sens.test_array_labels)
