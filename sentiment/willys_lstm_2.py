import tensorflow as tf
from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
from preprocessing.Tokenizers import process_df
from preprocessing.Tokenizers import bigram_df
from preprocessing.Tokenizers import df_to_tokenized_sentences
from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences

from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np

from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
from collections import defaultdict
import math
import matplotlib.pyplot as plt
from crowdcrafting import tasks_to_df, labeled_only_df

model=0#this is to be trained.
max_sequence=20#default.
num_dimensions=300

embedding_model=0


train_file = './datasets/facebook_part1.tsv'
test_file  = './datasets/facebook_part2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)

log.info("Tokenizing combined sentences")
crowd_df = labeled_only_df(tasks_to_df('./datasets/crowdcrafting/tasks_1.json',
                            './datasets/crowdcrafting/runs_1.json'))

train_df = pd.concat([crowd_df, tsv_to_dataframe(train_file), rpw_to_df('./datasets/fb4.tsv')], ignore_index=True)
train_df=labeled_only_df(train_df).reset_index()

test_df = tsv_to_dataframe(test_file)
test_df=labeled_only_df(test_df).reset_index()

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

#concatenate both train and test sentences for simplicity.
train_sentences= pd.concat([train_sentences,test_sentences],ignore_index=True)
log.info("Building Word2Vec")
# https://radimrehurek.com/gensim/models/word2vec.html
w2vmodel = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300

'''
#--------------------------------Train method --------------------------------------------------------
timesteps: number of default timesteps the LSTM has.
    epochs: how many epochs are carried out in learning
    batchSize: number of training samples used on every training.
    testSplit_frac= a float number that determines training-test split.
                            training samples = (total test + train) * testSplit_frac
                                    (rounded of course.)
'''
def train(train_sentences,embedding_model,avg_timestep=True,timesteps,split_fraction,shuffle=True,epochs,batchSize):
    #--------------------------MAKE THE TENSORFLOW MODEL ---------------------------.



    #----------Determine max timestep..---------------
    '''#We do this to ensure the optimal timestep tht represents
    #the majority of the data,
    #as most comments are in fact only 19 words long.
    '''

    # TODO: optimise timestep by using stdev, mean, tfidf, etc.---------------

    #---------------------------THIS IS IN THE OTHER FILE: ----------------------------------
    # willys_timestep_visualisation.py
    m = len(train_sentences['Text'])
    if avg_timestep==True:
        mxlen=0
        num=0
        for i in xrange(len(train_sentences['Text'])):
            num+=len(train_sentences['Text'][i])

        avg_timesteps=num/m
        print 'confirm timesteps. Press enter to confirm.'
        print 'average= '+str(avg_timesteps)
        print 'true avg:'+str(num/(m*1.0))
        max_sequence=avg_timesteps
    else:
        max_sequence=timesteps


    num_dimensions= 300


    #----------------------------------YOUR CODE ABOVE ----------------------

    #---------------------------------naive PRE preprocessing------------------------------

    '''
    'tidak': change to gak.
        'ga' = 295
        'gak'=250
        'gk'=104

    'kalau': change to kalau.
        'klo'=141
        'kalo'=126
        '''

    tidak_typos=['ga','gak','gk']
    kalau_typos=['klo','kalau']
    jadi_typos=['jd']
    for i in xrange(len(train_sentences['Text'])):

        for j in xrange(len(train_sentences['Text'][i])):
            if train_sentences['Text'][i][j] in tidak_typos:
                print 'sentence no :',i
                print train_sentences['Text'][i][j]
                train_sentences['Text'][i][j]='gak'
                print 'changed! to.......'
                print train_sentences['Text'][i][j]

                continue
            if train_sentences['Text'][i][j] in kalau_typos:
                print 'sentence no :',i
                print 'klo changed to kalo'
                train_sentences['Text'][i][j]='kalo'
                continue
            if train_sentences['Text'][i][j] in jadi_typos:
                print 'sentence no :',i
                print 'jadi typo'
                train_sentences['Text'][i][j]='jadi'
                continue



    #---------------------------GET TRAINING DATA -----------------------


    x_train=np.zeros((m,max_sequence,num_dimensions))


    y_train = np.zeros((m))


    for i in xrange(len(train_sentences['Text'])):
        timestep=0

        y_train[i]=train_sentences['Sentiment'][i]
        #y_train_vec[i][train_sentences['Sentiment'][i]+1]=1


        while timestep<max_sequence:
            try:

                if timestep>len(train_sentences['Text'][i])-1:
                    x_train[i][timestep]=np.zeros((num_dimensions))
                else:
                    vec=embedding_model[train_sentences['Text'][i][timestep]]
                    x_train[i][timestep]=vec


            except KeyError:
                #do nothing when the error is raised. (i.e. when someone types a typo).
                x_train[i][timestep]=np.zeros((num_dimensions))
            timestep+=1


    NUM_EXAMPLES =int( math.floor(m*(testSplit_frac)))#used to be 3.1/6
    test_input = x_train[NUM_EXAMPLES:]
    test_output = y_train[NUM_EXAMPLES:]
    x_train=x_train[:NUM_EXAMPLES]
    y_train = y_train[:NUM_EXAMPLES]
    #test_output_vec=y_train_vec[NUM_EXAMPLES:]
    print y_train
    print y_train.shape

    print 'train and test data loaded.'
    #------------------------SET TF TRAINING OPTIONS, PREDICTIONS, etc. ------------------

    trainY = to_categorical(y_train, nb_classes=3)
    testY = to_categorical(test_output, nb_classes=3)
    print 'Train/Test split:'
    print 'Train= '+ str(NUM_EXAMPLES) +', Test= '+str(m-NUM_EXAMPLES)


        # Network building
    net = tflearn.input_data([None,max_sequence,num_dimensions])

    net = tflearn.lstm(net, 256, dropout=0.8,return_seq=True)#old: 256
    net=tflearn.lstm(net,128,dropout=0.8)# old: 128

    net = tflearn.fully_connected(net, 3, activation='softmax')
    net = tflearn.regression(net, optimizer='adagrad', learning_rate=0.01#0.001,
                                , loss='categorical_crossentropy')#used to be adagrad

    print 'ready to train'
    # Training and testing using validation (test) set.
    model = tflearn.DNN(net, tensorboard_verbose=0)#,tensorboard_dir="logs"
    model.fit(x_train, trainY, n_epoch=epochs,validation_set=(test_input, testY), show_metric=True, batch_size=batchSize,shuffle=True)#old:10,40

def predictTokenizedSentence(tokenized_sentence):
    timestep=0
    w2v=np.zeros((1,max_sequence,num_dimensions))
    for word in tokenized_sentence:
        try:
            w2v[0][timestep]=embedding_model[word]
        except KeyError:
                print word +' not found.  Using default zeros instead.'
                #w2v[0][timestep]=np.zeros(num_dimensions)
        timestep+=1
    sentiment= model.predict_label(w2v)[0]-1
    if sentiment !=0:
        sentiment=sentiment *-1
    return sentiment

def predict(X):
    predictions=model.predict_label(X)

    dicts=[]
    dict={}


def predictModel(string):
    w2v= np.zeros((1,max_sequence,num_dimensions))
    timestep=0
    #assert string.split(' ')<max_sequence,'sentence too long dude.'
    for word in string.split(' '):

        try:
            w2v[0][timestep]=w2vmodel[word]
        except KeyError:
                print word +' not found.  Using default zeros instead.'
                #w2v[0][timestep]=np.zeros(num_dimensions)
        timestep+=1
    print model.predict(w2v)
    print model.predict_label(w2v)
#---------------------------------------------------------------------------------------
# 0 is positive, 1 is neutral, 2 is negative.


#-------------------TESTING CODE. USE THIS TO Train ----------------------------------


train(timesteps=20,epochs=40,batchSize=10,testSplit_frac=9.0/10)
