import fasttext
from preprocessing.Readers import tsv_to_fasttxt, \
                                  tsv_to_cleaned_fasttxt, \
                                  tsv_to_dataframe
from testing.Logger import log_prediction

# classifier = fasttext.supervised('./fasttext/fb_1.txt','ft_supervised')
# classifier = fasttext.load_model('./fasttext/fb_1_supervised.bin',
#    label_prefix='__label__')

#classifier = fasttext.load_model('./models/fb_1_pretrained.bin', encoding='utf-8')
trainfile_tsv = './datasets/facebook_part1.tsv'
trainfile_txt = './datasets/fb_1.txt'
testfile_tsv = './datasets/facebook_part2.tsv'
testfile_txt = './datasets/fb_2.txt'

tsv_to_cleaned_fasttxt(trainfile_tsv, trainfile_txt)
tsv_to_cleaned_fasttxt(testfile_tsv, testfile_txt)

classifier = fasttext.supervised(trainfile_txt,
                                './models/fb_1_pretrained_bin_half.bin',
                                pretrained_vectors = './models/wiki.id.vec',
                                dim=300)

test = tsv_to_dataframe(testfile_tsv)

x_test = test["Text"]
y_test = test["Sentiment"]

result = classifier.test(testfile_txt)
labels = classifier.predict(x_test)
label_int = [int(x[0]) for x in labels]

(accuracy, f1score, recall, precision, class_report, confusion) = log_prediction(y_test, label_int)

print 'P@1: ', result.precision
print 'R@1: ', result.recall
print 'Number of examples: ', result.nexamples

print ' Accuracy: %0.2f' % accuracy
print ' F1 score: %0.2f' % f1score
print ' Recall: %0.2f' % recall
print ' Precision: %0.2f' % precision
print ' clasification report:' 
print class_report
print ' confussion' 
print confusion
