''' DO NOT USE THIS CODE!!!


import tensorflow as tf
from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Readers import tsv_to_dataframe
from preprocessing.Tokenizers import process_df
from preprocessing.Tokenizers import bigram_df
from preprocessing.Tokenizers import df_to_tokenized_sentences
from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences
from build_models.build_models import build_word_vector
from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np
from sklearn.linear_model import LogisticRegression
from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
import tensorflow as tf
train_file = './datasets/facebook_part1.tsv'
test_file  = './datasets/facebook_part2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

log.info("Tokenizing combined sentences")
train_df = tsv_to_dataframe(train_file)
test_df = tsv_to_dataframe(test_file)

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

log.info("Building Word2Vec")
# https://radimrehurek.com/gensim/models/word2vec.html
model = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300


#--------------------------MAKE THE TENSORFLOW MODEL ---------------------------.


#-------------------------GET THE DATA -----------------------------------------
num_dimensions=300
max_sequence=500

m = len(train_sentences['Text'])
x_train=np.zeros((m,max_sequence,num_dimensions))

#TODO try to make y train an m x 3  vector for the sentiment.
#-----------NEW EDIT -------------
#y_train=np.zeros((m,3))
#-------------------------------

y_train = np.zeros((m))
y_train_vec= np.zeros((m,3))
#[m][timestep][num_dimensions]

for i in xrange(len(train_sentences['Text'])):
    timestep=0
    #----------NEW EDIT ---------------
    # y_train[i][train_sentences['Sentiment'][i+1]]=1
    #----------------------------------

    y_train[i]=train_sentences['Sentiment'][i]
    y_train_vec[i][train_sentences['Sentiment'][i]+1]=1

    for word in train_sentences['Text'][i]:
        #do something!
        try:
            vec=model[word]
            x_train[i][timestep]=vec
        except KeyError:
            #do nothing when the error is raised. (i.e. when someone types a typo).
            x_train[i][timestep]=np.zeros((300))
        timestep+=1
    if timestep< max_sequence-1:
        for j in xrange(timestep,max_sequence):
            x_train[i][timestep]=np.zeros((300))
            timestep+=1
#
NUM_EXAMPLES = 800
test_input = x_train[NUM_EXAMPLES:]
test_output = y_train[NUM_EXAMPLES:]
x_train=x_train[:NUM_EXAMPLES]
y_train = y_train[:NUM_EXAMPLES]
test_output_vec=y_train_vec[NUM_EXAMPLES:]

#------------------------SET TF TRAINING OPTIONS, PREDICTIONS, etc. ------------------

data = tf.placeholder(tf.float32, [None,max_sequence,num_dimensions])
target = tf.placeholder(tf.float32, [m-NUM_EXAMPLES,3])


num_hidden = 200
cell = tf.nn.rnn_cell.LSTMCell(num_hidden,state_is_tuple=True)
val, state = tf.nn.dynamic_rnn(cell, data, dtype=tf.float32)

val = tf.transpose(val, [1, 0, 2])
last = tf.gather(val, int(val.get_shape()[0]) - 1)

num_classes=3
weight = tf.Variable(tf.truncated_normal([num_hidden, num_classes]))
#-------------NEW EDIT------------------------------------------
#bias = tf.Variable(tf.constant(0.1, shape=[3]))
#---------------------------------------------------------------
bias= tf.Variable(tf.constant(0.1,shape=[target.get_shape()[1]]))
prediction = tf.nn.softmax(tf.matmul(last, weight) + bias)

cross_entropy = -tf.reduce_sum(target * tf.log(tf.clip_by_value(prediction,1e-10,1.0)))

optimizer = tf.train.AdamOptimizer()
minimize = optimizer.minimize(cross_entropy)

mistakes = tf.not_equal(tf.argmax(target, 1), tf.argmax(prediction, 1))
error = tf.reduce_mean(tf.cast(mistakes, tf.float32))


#----------------------TRAIN THE MODEL!---------- --------------------------------------

init_op = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init_op)
#raw_input('train samples:  '+str(len(x_train)))
batch_size = 1000 # this used to be 1000.
no_of_batches = int(len(x_train)/batch_size)
epoch = 5000
for i in range(epoch):
    ptr = 0
    for j in range(no_of_batches):
        inp, out = x_train[ptr:ptr+batch_size], y_train[ptr:ptr+batch_size]
        ptr+=batch_size
        sess.run(minimize,{data: inp, target: out})
    print "Epoch - ",str(i)
raw_input('training done')
incorrect = sess.run(error,{data: test_input, target: test_output_vec})
print('Epoch {:2d} error {:3.1f}%'.format(i + 1, 100 * incorrect))

#-----------------------TEST THE MODEL ---------------------------------
vek= np.zeros((2,500,300))
vek[0]=test_input[10]
vek[1]=test_input[11]
print vek[0]
print vek[1]
print sess.run(prediction,{data:vek})
print 'actual'
print test_output[10],test_output[11]
sess.close()
'''
