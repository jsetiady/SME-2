import numpy
from sklearn.naive_bayes import GaussianNB
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_score, \
         recall_score, confusion_matrix, classification_report, \
         accuracy_score, f1_score
from sklearn.model_selection import cross_val_score, RandomizedSearchCV, \
     GridSearchCV
from testing.Logger import logger, log_to_textfile
import scipy

# hyperparameter optimization
# works only for forest
def best_params(est_name, x_data, y_data):
    est_key = {'maxent' : LogisticRegression(solver='newton-cg'), 
               'forest' : RandomForestClassifier(),
               'MLP' : MLPClassifier()}
    est = est_key[est_name]

    maxent_grid = {'C' : [1,10,100,1000]}
    """
                'class_weight' : ['balanced',None],
                'solver' : ['newton-cg', 'lbfgs', 'liblinear', 'sag']}
    """
    forest_grid = {'n_estimators' : [1,10,100,1000]}
    mlp_grid = {'activation' : ['tanh','relu']}
    grid_key = {'maxent': maxent_grid, 
                'forest': forest_grid, 
                'MLP': mlp_grid}
    grid = grid_key[est_name]
    print "Setting up clf"
    clf = GridSearchCV(estimator = est, param_grid = grid, 
                             n_jobs = -1, error_score = 0)
    print "Fitting clf"
    clf.fit(x_data,y_data)
    print "Returning best params"
    return clf.best_params_

def log_all_classifier_tests(log_file,file_train,file_test,x_train,y_train,x_test,y_test):
    # log_file = open(f,'w')
    log_file.write('File Train : ' + file_train)
    log_file.write('\nFile Test : ' + file_test)
    # log_file.write('\nFile Data : ' + file_data + '\n')

    maxent = LogisticRegression(solver='newton-cg')
    # maxent = LogisticRegressionCV()
    maxent.fit(x_train, y_train)

    forest = RandomForestClassifier(n_estimators=40,max_leaf_nodes=15,max_depth=6,min_samples_leaf=5)
    # forest = RandomForestClassifier()
    forest = forest.fit(x_train, y_train)

    naiveb = GaussianNB()
    naiveb = naiveb.fit(x_train, y_train)

    svecm = svm.SVC()
    svecm = svecm.fit(x_train, y_train)

    mlp = MLPClassifier(solver='adam', alpha=1e-5,
                    hidden_layer_sizes=(5, 2), random_state=1,
                    activation='tanh')
    mlp.fit(x_train, y_train)
 
    maxent_prediction, maxent_log = logger(maxent, 10,
        x_train, y_train, 
        x_test, y_test)
    log_to_textfile(log_file, 'Maximum Entropy', maxent_log) 

    forest_prediction, forest_log = logger(forest, 10,
        x_train, y_train,
        x_test, y_test)
    log_to_textfile(log_file, 'Random Forest', forest_log)

    naiveb_prediciton, naiveb_log = logger(naiveb, 10,
        x_train, y_train,
        x_test, y_test)
    log_to_textfile(log_file, 'Naive Bayes', naiveb_log)
        
    svecm_prediction, svecm_log = logger(svecm, 10,
        x_train, y_train,
        x_test, y_test)
    log_to_textfile(log_file, 'SVM', svecm_log)

    mlp_prediction, mlp_log = logger(mlp, 10,
        x_train, y_train,
        x_test, y_test)
    log_to_textfile(log_file, 'MLP', mlp_log)

    # log_file.close()

def data_vs_size_classifiers(f,file_train,file_test,x_train,y_train,
                            x_test,y_test):
    tot_train = len(x_train)
    tot_test = len(x_test)
    f.write("\nTOTAL TRAINING DATA SIZE: " + str(tot_train) + "\n")
    for i in xrange(1,5):
        curr_size = i*(tot_train/4)
        print i
        print "========================================"
        f.write("\nDATA SIZE: " + str(curr_size) + "\n")
        log_all_classifier_tests(f,file_train,file_test,
                                x_train[0:curr_size],y_train[0:curr_size],
                                x_test,y_test)


