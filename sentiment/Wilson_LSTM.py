import tensorflow as tf
from gensim.models.keyedvectors import KeyedVectors
#from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
#from preprocessing.Tokenizers import process_df
#from preprocessing.Tokenizers import bigram_df
#from preprocessing.Tokenizers import df_to_tokenized_sentences
#from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences
from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np
import cPickle as pickle
from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
from collections import defaultdict
import math
# import matplotlib.pyplot as plt
from preprocessing.crowdcrafting import tasks_to_df, labeled_only_df

class Wilson_LSTM:
    def __init__(self,embedding_model,num_dimensions,max_seq=20,load=False):
        self.embedding_model=embedding_model
        self.num_dimensions=num_dimensions
        # self.sentences=sentences
        # self.lstm_model_name=lstm_model_name
        self.max_sequence=max_seq
    #def initialise():



    def trainNewModel(self,timesteps,split_fraction,epochs,batchSize, sentences, avg_timestep=True,isShuffle=True):
        #--------------------------MAKE THE TENSORFLOW MODEL ---------------------------.


        #----------Determine max timestep..---------------
        '''#We do this to ensure the optimal timestep tht represents
        #the majority of the data,
        #as most comments are in fact only 19 words long.
        '''

        # TODO: optimise timestep by using stdev, mean, tfidf, etc.---------------

        #---------------------------THIS IS IN THE OTHER FILE: ----------------------------------
        # willys_timestep_visualisation.py
        self.sentences = sentences
        m = len(self.sentences['Text'])
        if avg_timestep==True:
            mxlen=0
            num=0
            for i in xrange(len(self.sentences['Text'])):
                num+=len(self.sentences['Text'][i])

            avg_timesteps=num/m
            print 'confirm timesteps. Press enter to confirm.'
            print 'average= '+str(avg_timesteps)
            print 'true avg:'+str(num/(m*1.0))
            self.max_sequence=avg_timesteps
        else:
            self.max_sequence=timesteps


        #self.num_dimensions= 300



        #---------------------------------naive PRE preprocessing TYPOS------------------------------

        '''
        'tidak': change to gak.
            'ga' = 295
            'gak'=250
            'gk'=104

        'kalau': change to kalau.
            'klo'=141
            'kalo'=126
            '''

        tidak_typos=['ga','gak','gk']
        kalau_typos=['klo','kalau']
        jadi_typos=['jd']
        senang_typos=['senang']
        for i in xrange(len(self.sentences['Text'])):

            for j in xrange(len(self.sentences['Text'][i])):
                if self.sentences['Text'][i][j] in tidak_typos:
                    print 'sentence no :',i
                    print self.sentences['Text'][i][j]
                    self.sentences['Text'][i][j]='gak'
                    print 'changed! to.......'
                    print self.sentences['Text'][i][j]
                    continue

                if self.sentences['Text'][i][j] in kalau_typos:
                    print 'sentence no :',i
                    print 'klo changed to kalo'
                    self.sentences['Text'][i][j]='kalo'
                    continue
                if self.sentences['Text'][i][j] in jadi_typos:
                    print 'sentence no :',i
                    print 'jadi typo'
                    self.sentences['Text'][i][j]='jadi'
                    continue
                if self.sentences['Text'][i][j] in senang_typos:
                    print 'sentence no :',i
                    print 'senang typo'
                    self.sentences['Text'][i][j]="senang"
                    continue
        #---------------------------GET TRAINING DATA -----------------------


        x_train=np.zeros((m,self.max_sequence,self.num_dimensions))
        y_train = np.zeros((m))


        for i in xrange(len(self.sentences['Text'])):
            timestep=0

            y_train[i]=self.sentences['Sentiment'][i]

            while timestep<self.max_sequence:
                try:
                    if timestep>len(self.sentences['Text'][i])-1:
                        x_train[i][timestep]=np.zeros((self.num_dimensions))
                    else:
                        vec=self.embedding_model[self.sentences['Text'][i][timestep]]
                        x_train[i][timestep]=vec
                except KeyError:
                    #do nothing when the error is raised. (i.e. when someone types a typo).
                    x_train[i][timestep]=np.zeros((self.num_dimensions))
                timestep+=1


        NUM_EXAMPLES =int( math.floor(m*(split_fraction)))#used to be 3.1/6
        test_input = x_train[NUM_EXAMPLES:]
        test_output = y_train[NUM_EXAMPLES:]
        x_train=x_train[:NUM_EXAMPLES]
        y_train = y_train[:NUM_EXAMPLES]

        print y_train
        print y_train.shape

        print 'train and test data loaded.'
        #------------------------SET TF TRAINING OPTIONS, PREDICTIONS, etc. ------------------

        trainY = to_categorical(y_train, nb_classes=3)
        testY = to_categorical(test_output, nb_classes=3)
        print 'Train/Test split:'
        print 'Train= '+ str(NUM_EXAMPLES) +', Test= '+str(m-NUM_EXAMPLES)


            # Network building
        net = tflearn.input_data([None,self.max_sequence,self.num_dimensions])

        net = tflearn.lstm(net, 256, dropout=0.8,return_seq=True)#old: 256
        net=tflearn.lstm(net,128,dropout=0.8)# old: 128

        net = tflearn.fully_connected(net, 3, activation='softmax')
        net = tflearn.regression(net, optimizer='adagrad', learning_rate=0.01#0.001,
                                    , loss='categorical_crossentropy')#used to be adagrad

        print 'ready to train'
        # Training and testing using validation (test) set.
        self.model = tflearn.DNN(net, tensorboard_verbose=0)#,tensorboard_dir="logs"
        self.model.fit(x_train, trainY, n_epoch=epochs,validation_set=(test_input, testY), show_metric=True, batch_size=batchSize,shuffle=isShuffle)#old:10,40

    def fitNewData(self,newX,newY,testSplit_frac):
            NUM_EXAMPLES =int( math.floor(len(newX)*(testSplit_frac)))#used to be 3.1/6
            test_input = newX[NUM_EXAMPLES:]
            test_output = newY[NUM_EXAMPLES:]
            x_train=newX[:NUM_EXAMPLES]
            y_train = newY[:NUM_EXAMPLES]
            trainY = to_categorical(y_train, nb_classes=3)
            testY = to_categorical(test_output, nb_classes=3)

            self.model.fit(x_train, trainY, n_epoch=epochs,validation_set=(test_input, testY), show_metric=True, batch_size=batchSize,shuffle=True)#old:10,40

    def predictTokenizedSentence(self,tokenized_sentence):
        timestep=0
        w2v=np.zeros((1,self.max_sequence,self.num_dimensions))
        for word in tokenized_sentence:
            if timestep >= self.max_sequence:
                break
            try:
                w2v[0][timestep]=self.embedding_model[word]
            except KeyError:
                    print word +' not found.  Using default zeros instead.'

            timestep+=1
        sentiment= self.model.predict_label(w2v)[0][0]-1
        #TODO WHY DOES THERE HAVE TO BE 2 indexes?
        #print sentiment
        if not (sentiment ==0):
            sentiment=sentiment *(-1)
        return sentiment


    def predictString(self,string):
        #presumably delimited by spaces.
        return self.predictTokenizedSentence(string.split(" "))

    def save(self, fname):
        #use self.lstm_model_name
        # self.model.save(self.lstm_model_name)
        #pickle.dump(self.model, open( self.lstm_model_name+".p", "w+" ) )
        self.model.save(fname)
        print "model saved."

    def load(self, fname):
        #make the layer.
        tf.reset_default_graph()

        netz = tflearn.input_data([None,self.max_sequence,self.num_dimensions])
        netz = tflearn.lstm(netz, 256, dropout=0.8,return_seq=True)#old: 256
        netz=tflearn.lstm(netz,128,dropout=0.8)# old: 128
        netz = tflearn.fully_connected(netz, 3, activation='softmax')
        netz = tflearn.regression(netz, optimizer='adagrad', learning_rate=0.01#0.001,
                                    , loss='categorical_crossentropy')#used to be adagrad
        self.model = tflearn.DNN(netz, tensorboard_verbose=0)#,tensorboard_dir="logs
        """
        try:
            if lstm_name=="":
                #self.model=pickle.load(open(self.lstm_model_name+".p"))
                self.model.load(self.lstm_model_name)

            else:
            #self.model=pickle.load(open(lstm_name+".p"))
                self.model.load(lstm_name)
            print "model loaded."
        """
        try:
            self.model.load(fname,True)
            print "LSTM model loaded"
        except IOError:
            print "failed to load!"
