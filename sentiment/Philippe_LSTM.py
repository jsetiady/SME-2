from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df
from keras.models import *
from keras.layers import *
# import matplotlib.pyplot as plt
from keras.optimizers import *
import numpy as np
import pandas as pd
from Attention import *
from preprocessing.crowdcrafting import tasks_to_df, labeled_only_df
import math
from keras.callbacks import *
from keras.models import model_from_json
import json

class Philippe_LSTM:

    def __init__(self, embedding_model, num_dimensions):
        self.embedding_model = embedding_model
        self.dimensions = num_dimensions

    def trainNewModel(self, sentences, split_fraction, epochs,
                        timesteps=37,
                        batch_size=80,
                        avg_timestep = True,
                        shuffle = True,
                        reg = 0.01,
                        rdropout = 0.03,
                        dropout = 0.5,
                        learning_rate = 0.01,
                        clipvalue = 0.1):

        self.sentences = sentences

        m = len(self.sentences)

        if(avg_timestep == True):
            totallength=0
            for p in range(m):
                totallength += len(self.sentences["Text"][p])
            self.timesteps1 = int(math.floor(totallength/m*1.0))
        else:
            print timesteps
            self.timesteps1 = timesteps

        print "timesteps: "+ str(self.timesteps1)
        print "dimensions: "+ str(self.dimensions)

#-------------------------------------------------------------------------------
#   Creating model
#-------------------------------------------------------------------------------

        self.model = Sequential()
        self.model.add(Masking(mask_value=0, input_shape=(self.timesteps1, self.dimensions)))
        self.model.add(Bidirectional(LSTM(self.dimensions,  input_shape=(self.timesteps1, self.dimensions),
                                    return_sequences=True,
                                    kernel_regularizer=regularizers.l1_l2(reg),
                                    recurrent_dropout=rdropout,
                                    dropout=dropout
                                    )))
        self.model.add(Bidirectional(LSTM( self.dimensions,  input_shape=(self.timesteps1, self.dimensions),
                                    return_sequences=True,
                                    kernel_regularizer=regularizers.l1_l2(reg),
                                    recurrent_dropout=rdropout,
                                    dropout=dropout
                                    )))
        self.model.add(Attention())
        self.model.add(Dense(3, activation='softmax'))
        self.model.compile(loss="categorical_crossentropy",
                        optimizer=Adagrad(lr=learning_rate, clipvalue=clipvalue, epsilon=1e-9),
                        metrics=['accuracy'])
        print(self.model.summary())

        self.fitNewData( sentences, split_fraction, epochs, batch_size, shuffle)

#-------------------------------------------------------------------------------
#   fit with new data
#-------------------------------------------------------------------------------

    def fitNewData(self, sentences, testSplit_frac, epochs,
                   timesteps=37, batch_size=80,
                   shuffle = True, avg_timestep = True, plot=True):

        self.sentences = sentences
        self.sentences = self.sentences.sample(frac=1)

        m = len(self.sentences)

        if(avg_timestep == True):
            totallength=0
            for p in range(m):
                totallength += len(self.sentences["Text"][p])
            self.timesteps1 = int(math.floor(totallength/m))
        else:
            self.timesteps1 = timesteps


        #print "timesteps: "+ str(self.timesteps1)
#-------------------------------------------------------------------------------
#   Correcting Spelling
#-------------------------------------------------------------------------------

        tidak_typos=['ga','gak','gk']
        kalau_typos=['klo','kalau']
        jadi_typos=['jd']

        for i in xrange(len(self.sentences['Text'])):
            for j in xrange(len(self.sentences['Text'][i])):
                if self.sentences['Text'][i][j] in tidak_typos:
                    #print 'sentence no :',i
                    #print self.sentences['Text'][i][j]
                    self.sentences['Text'][i][j]='gak'
                    #print 'changed! to.......'
                    #print self.sentences['Text'][i][j]

                    continue
                if self.sentences['Text'][i][j] in kalau_typos:
                    #print 'sentence no :',i
                    #print 'klo changed to kalo'
                    self.sentences['Text'][i][j]='kalo'
                    continue
                if self.sentences['Text'][i][j] in jadi_typos:
                    #print 'sentence no :',i
                    #print 'jadi typo'
                    self.sentences['Text'][i][j]='jadi'
                    continue

#-------------------------------------------------------------------------------
#   Loading data
#-------------------------------------------------------------------------------

        x_train = np.zeros([m,self.timesteps1,self.dimensions])
        y_train = np.zeros([m,3])

        for i in range(m):
            b = self.sentences["Text"][i]
            for j in range(min(len(b),self.timesteps1)):
                try:
                    x_train[i][j] = self.embedding_model[self.sentences["Text"][i][j]]
                except KeyError:
                    x_train[i][j] = np.zeros((300))
                    continue
            for k in range(min(len(b),self.timesteps1),self.timesteps1):
                x_train[i][k] = np.zeros((300))
            y_train[i] = [0,0,0]
            y_train[i][self.sentences["Sentiment"][i]+1]=1

#-------------------------------------------------------------------------------
#   Spliting between training and test
#-------------------------------------------------------------------------------

        TRAIN_EXAMPLES=int(math.floor(m*(testSplit_frac)))
        x_test=x_train[TRAIN_EXAMPLES:]
        y_test=y_train[TRAIN_EXAMPLES:]
        x_train=x_train[:TRAIN_EXAMPLES]
        y_train=y_train[:TRAIN_EXAMPLES]

        h = self.model.fit(x_train, y_train, epochs=epochs,  batch_size=batch_size,
                    verbose = 1, shuffle=True, validation_data=(x_test,y_test)
                    , callbacks=[EarlyStopping(monitor='val_loss',
                                              patience=7, verbose=0, mode='auto')])

#-------------------------------------------------------------------------------
#   Printing Output
#-------------------------------------------------------------------------------

        np.set_printoptions(threshold='nan')

        print "self.model output vs actual output"
        print np.hstack((self.model.predict(x_test),y_test))
        #print(h.history.keys())

#-------------------------------------------------------------------------------
#   Graphing Output
#-------------------------------------------------------------------------------
    """
        if plot==True:
            plt.plot(h.history['acc'])
            plt.plot(h.history['val_acc'])
            plt.title('self.model accuracy')
            plt.ylabel('accuracy')
            plt.xlabel('epoch')
            plt.legend(['train', 'test'], loc='upper left')
            plt.show()
    """
#-------------------------------------------------------------------------------
#   predict sentiment of a string
#-------------------------------------------------------------------------------

    def predictString(self, string):
        return self.predictTokenizedSentence(string.split(' '))

#-------------------------------------------------------------------------------
#   predict sentiment of a tokenized sentence
#-------------------------------------------------------------------------------

    def predictTokenizedSentence(self, tokenized_sentence):
        testW2v = np.zeros((1,self.timesteps1,self.dimensions))
        timestep=0
        for word in tokenized_sentence:
            if timestep >= self.timesteps1:
                break
            try:
                wordvec =self.embedding_model[word]
                testW2v[0][timestep-1]=wordvec
            except KeyError:
                print word+ " not found"
                testW2v[0][timestep-1] = np.zeros((300))
                continue
            timestep+=1

        a = self.model.predict(testW2v)

        print "sentiment: "+ str(a[0].tolist().index(max(a[0]))-1)
        return str(a[0].tolist().index(max(a[0]))-1)

#-------------------------------------------------------------------------------
#   Save Model
#-------------------------------------------------------------------------------

    def save(self, lstm_model_path):
        self.lstm_model_name = lstm_model_path
        self.model.save(self.lstm_model_name + '.h5')

#-------------------------------------------------------------------------------
#   Load Model
#-------------------------------------------------------------------------------

    def load(self, lstm_model_path):
        self.lstm_model_name = lstm_model_path
        self.model = load_model(self.lstm_model_name + '.h5', custom_objects={'Attention': Attention()})
        json_string = self.model.to_json()
        json_string1 = json.loads(json_string)
        self.timesteps1 = json_string1["config"][0]["config"]["batch_input_shape"][1]
        self.dimensions = json_string1["config"][0]["config"]["batch_input_shape"][2]
