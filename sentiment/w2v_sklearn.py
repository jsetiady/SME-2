from crowdcrafting import tasks_to_df, labeled_only_df
from preprocessing.Readers import tsv_to_dataframe, rpw_to_df, shuffle
from preprocessing.Tokenizers import process_df
from preprocessing.Tokenizers import bigram_df
from preprocessing.Tokenizers import df_to_tokenized_sentences
from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences
from build_models.build_models import build_sentence_vector
from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np
from sklearn.linear_model import LogisticRegression
from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests, best_params
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
import pandas as pd
import logging
import sys

even_more_unsup = './datasets/tweets_large(18k).tsv'
more_unsup_file = './datasets/fb_3(45k).tsv'
unsup_train_file = './datasets/fb_large(14k).tsv'
# unsup_train_file = './datasets/bahasa_tweets.tsv'
train_file = './datasets/facebook_part1.tsv'
test_file  = './datasets/facebook_part2.tsv'
# test_file = './datasets/fb_a2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

log.info("Loading tsv files")
even_more_unsup_df = tsv_to_dataframe(even_more_unsup)
more_unsup_df = tsv_to_dataframe(more_unsup_file)
unsup_train_df = tsv_to_dataframe(unsup_train_file)

log.info("Tokenizing train sentences")
train_df = tsv_to_dataframe(train_file)
even_more_train_df = rpw_to_df('./datasets/fb4.tsv')
# train_sentences = df_to_tokenized_2gram_sentences(train_df)

log.info("Tokenizing test sentences")
test_df = tsv_to_dataframe(test_file)
# test_sentences = df_to_tokenized_2gram_sentences(test_df)

log.info("Tokenizing combined sentences")
more_train_df = labeled_only_df(tasks_to_df('./datasets/crowdcrafting/tasks_1.json',
                            './datasets/crowdcrafting/runs_1.json'))
train_df = pd.concat([train_df, even_more_train_df], ignore_index=True)
test_df = tsv_to_dataframe(test_file)
unsup_train_df = pd.concat([train_df,unsup_train_df,even_more_unsup_df,more_unsup_df],
    ignore_index=True)

unsup_train_sentences = bigram_df(process_df(unsup_train_df))

all_df = pd.concat([train_df,test_df], ignore_index=True)
all_df = shuffle(all_df)
all_sup_sentences = bigram_df(process_df(all_df))
sup_test_sentences = labeled_only_df(all_sup_sentences.head(900)).reset_index()
sup_train_sentences = labeled_only_df(all_sup_sentences.tail(len(all_sup_sentences)-900)).reset_index()

'''
unsup_train_sentences = process_df(combined_train_df)
sup_train_sentences = process_df(train_df)
sup_test_sentences = process_df(test_df)

sup_train_sentences = df_to_tokenized_2gram_sentences(train_df)
sup_test_sentences = df_to_tokenized_2gram_sentences(test_df)
unsup_train_sentences = df_to_tokenized_2gram_sentences(combined_train_df)
'''

log.info("Building Word2Vec")
# https://radimrehurek.com/gensim/models/word2vec.html
model_size = 300
# model = Word2Vec(unsup_train_sentences["Text"], size=model_size,
#                 workers=2,iter=5,window=5,sg=1,hs=1,min_count=5)
# model.wv.save('./models/lstm.bin')
model = KeyedVectors.load('./models/lstm.bin')
'''
this is to re-train using new sentence stream:
 model.train(train_sentences, total_examples=1000, epochs=model.iter)

remember to convert model --> KeyedVector to save RAM after training is done
'''
log.info("Building word vectors")
train_num = len(sup_train_sentences)
test_num = len(sup_test_sentences)

train_array = np.zeros((train_num,model_size))
test_array = np.zeros((test_num,model_size))

for i in xrange(0,train_num):
    train_array[i] = build_sentence_vector(model,sup_train_sentences["Text"][i],model_size)

for i in xrange(0,test_num):
    test_array[i] = build_sentence_vector(model,sup_test_sentences["Text"][i],model_size)

train_array_labels = sup_train_sentences["Sentiment"].as_matrix()
test_array_labels = sup_test_sentences["Sentiment"].as_matrix()

#this is where we come in .

log.info("Classifying and graphing")

"""
# optimize with CV Grid Search
maxent = LogisticRegression(C=10)
maxent.fit(train_array,train_array_labels)
print "C: 10, Acc: "+str(maxent.score(test_array,test_array_labels))
maxent = LogisticRegression(C=100)
maxent.fit(train_array,train_array_labels)
print"C: 100, Acc: "+str(maxent.score(test_array,test_array_labels))
"""

"""
maxent.fit(train_array,sup_train_sentences["Sentiment"])
joblib.dump(maxent, './models/lstmbin_maxent.pkl')
"""
"""
best = best_params('maxent', 
                   train_array, 
                   train_array_labels)
"""
"""
grid = [{'n_estimators': [1,10,100]}]
clf = GridSearchCV(estimator = RandomForestClassifier(), 
                   param_grid = [{'n_estimators' : [1,10,100]}],
                   error_score=0)
"""
maxent = LogisticRegression()
graph_vs_datasize('w2v_sklearn_hyper', maxent, 10, 'accuracy',
                    train_array, sup_train_sentences["Sentiment"],
                    test_array, sup_test_sentences["Sentiment"])

log.info("Save into textfile")
f = open('./logs/w2v_sklearn_hyper.txt', 'w')
log_all_classifier_tests(f,
                        train_file,
                        test_file,
                        train_array,sup_train_sentences["Sentiment"],
                        test_array,sup_test_sentences["Sentiment"])
f.close()
