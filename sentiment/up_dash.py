"""
from sklearn.externals import joblib
from preprocessing.Readers import tsv_to_dataframe
from preprocessing.Tokenizers import process_df, bigram_df
from build_models.build_models import build_sentence_vector
from gensim.models.keyedvectors import KeyedVectors
import pandas as pd
import numpy as np
"""

from master import word_vectors, sentences_df, classifier

def up_dash(in_file, out_file, clf_file='./models/lstmbin_maxent.pkl', 
                     wvm_file='./models/lstm.bin', wvm_type='keyedvectors', wvm_size=300, 
                     speech_type=True, text=False, product=False):
    wvm = word_vectors('keyedvectors')
    wvm.load(wvm_file)

    clf = classifier('maxent', wvm.model, wvm_size)
    clf.load(clf_file)
    clf.classify_tsv(in_file, out_file, speech_type=speech_type)

def up_dash_old(in_file, out_file, clf_file = './models/lstmbin_maxent.pkl',
            model_file = './models/lstm.bin', model_size=300):
    model = KeyedVectors.load('./models/lstm.bin')
    print "Model loaded"
    clf = joblib.load('./models/lstmbin_maxent.pkl')
    print "Clf loaded"
    df = tsv_to_dataframe(in_file)
    print "Begin process df"
    proc_df = bigram_df(process_df(df))
    x_num = len(proc_df)
    dicts = []
    for i in xrange(0,x_num):
        x = build_sentence_vector(model, proc_df["Text"][i], model_size)
        y = int(clf.predict(x))
        dict = {"ID" : proc_df["ID"][i], "Sentiment" : y, "Speech_Type": 9}
        dicts.append(dict)
    out_df = pd.DataFrame(dicts)
    out_df.to_csv(out_file, sep = '\t')

