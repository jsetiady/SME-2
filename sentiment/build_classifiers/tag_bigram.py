import pandas as pd

def load_tag_dict(tag_source):

    tag_table = pd.read_csv(tag_source, header=0,
                            delimiter=",", quoting=3)

    tag_dict = {}
    
    for i in xrange(tag_table["Word"].size):
        
        word = lowercase_all(tag_table["Word"][i])
        tag = lowercase_all(tag_table["Tag"][i])

        tag_dict[tag_table["Word"][i]] = tag_table["Tag"][i]

    return tag_dict


def tag_bigram(bigram, tag_dict):
    
    tags = []
    
    for phrase in bigram:
        if phrase in tag_dict:
            tags.append(tag_dict[phrase])

    return tags

