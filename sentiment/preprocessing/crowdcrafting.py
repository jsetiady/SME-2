import operator
import pandas as pd
from Readers import tsv_to_dataframe, csv_to_dataframe, json_to_df

def tsv_to_tasks(in_file, out_file, num_tasks):
  df = tsv_to_dataframe(in_file)
  dicts = []

  i = 0
  j = 0

  while i<len(df) and j<num_tasks:
    sentence = str(df["Text"][i])
    if len(sentence) > 5:
      dict = {"Text" : sentence}
      dicts.append(dict)
      j += 1
    i += 1
  crowdcraft = pd.DataFrame(dicts)
  crowdcraft.to_csv(out_file)
  return crowdcraft

def csv_to_tasks(in_file, out_file, num_tasks):
  df = csv_to_dataframe(in_file)
  dicts = []

  i = 0
  j = 0

  while i<len(df) and j<num_tasks:
    sentence = str(df["Text"][i])
    if len(sentence) > 5:
      dict = {"Text" : sentence}
      dicts.append(dict)
      j += 1
    i += 1
  crowdcraft = pd.DataFrame(dicts)
  crowdcraft.to_csv(out_file)
  return crowdcraft

def tasks_to_df(task_file, runs_file):
    sentiment_key = {'Negatif' : -1, 'Netral' : 0, 'Positif' : 1, 
                     'Ambigu / Tidak yakin / Tidak tentang Go-jek' : 8,
                     'Unlabeled' : 9}
    tasks = json_to_df(task_file)
    runs = json_to_df(runs_file)
    dicts = []
    for i in xrange(0,len(tasks)):
        task_id = tasks["id"][i]
        text = str(tasks["info"][i]["Text"])

        runs_of_task = runs.loc[runs["task_id"] == task_id]
        if len(runs_of_task):
            sentiment = sentiment_key[str(runs_of_task["info"].value_counts().idxmax())]
        else:
            sentiment = sentiment_key['Unlabeled']
        dict = {'ID' : task_id, 'Text' : text, 'Sentiment' : sentiment}
        dicts.append(dict)

    df = pd.DataFrame(dicts)
    return df

def labeled_only_df(tasks_df):
    return tasks_df.loc[operator.and_(operator.and_(tasks_df["Sentiment"] != 9, 
                                      tasks_df["Sentiment"] != 8),
                                      tasks_df["Sentiment"] != 7)].reset_index()


# tasks = tasks_to_df('./datasets/crowdcrafting/tasks_1.json','./datasets/crowdcrafting/runs_1.json')
# labeled_data = tasks.loc[operator.and_(tasks["Sentiment"] != 9, tasks["Sentiment"] != 8)]

"""
<span class="step step-0 question">Apa jenis ucapan ini?</span>

<div class="step step-0">
      	<button class="btn btn-answer step step-0" value="Question">Question</button>
      	<button class="btn btn-answer step step-0" value="Request/Saran">Request/Saran</button>
      	<button class="btn btn-answer step step-0" value="Statement">Statement</button>
        <br><br>
        <button class="btn btn-answer step step-0" value="Spam">Spam / Iklan</button>
        <button class="btn btn-answer step step-0" value="Tagging">Tagging</button>
      </div>

<button class="btn btn-skip step step-1" value="Skip">Skip</button>
"""
