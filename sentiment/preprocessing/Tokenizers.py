from nltk.corpus import stopwords
# from sentiment.preprocessing.Cleaners import clean
from Cleaners import clean #preprocessing.Cleaners
from gensim.models import Phrases
from gensim.models.phrases import Phraser
import gensim
import pandas as pd

def tokenize(unsplit_text):
    tokens = unsplit_text.split()
    return tokens

# stopwords_file run local: "bahasa"
def remove_stopwords(tokens, stopwords_file="../../../../../bahasa"):
    stops = set(stopwords.words(stopwords_file))
    tokens_excluding_stopwords = [t for t in tokens if t not in stops]
    return tokens_excluding_stopwords

def tokens_to_string(tokens):
    string_of_tokens_separated_by_spaces = " ".join(tokens)
    return string_of_tokens_separated_by_spaces

def tokens_to_bigrams(phraser, tokens):
    for token in phraser[tokens]:
        if token not in tokens:
            tokens.append(token)
    return tokens

def process_sentence(sentence, remove_punctuation=True, fix_typos=False):
    processed = clean(sentence, remove_punctuation, fix_typos)
    tokens = tokenize(processed)
    tokens = remove_stopwords(tokens)
    return tokens

def process_df(df, remove_punctuation=True, fix_typos=False):
    processed_list = []
    for i in xrange(0,len(df)):
        sentence = process_sentence(df["Text"][i], remove_punctuation, fix_typos)
        if sentence:
            processed_dict = {"ID" : df["ID"][i],
                                "Text" : sentence,
                                "Sentiment" : df["Sentiment"][i]}
            processed_list.append(processed_dict)
    return pd.DataFrame(processed_list)

def bigram_df(df):
    phrases = Phrases(df["Text"],min_count=1,threshold=2)
    bigramer = Phraser(phrases)
    for i in xrange(0,len(df)):
        bigrams = tokens_to_bigrams(bigramer,df["Text"][i])
        df.set_value(i, "Text", bigrams)
    return df

def df_to_tokenized_sentences(df):
    tokenized_sentences = []
    for i in xrange(0,df["Text"].size):
        tokens = process_sentence(df["Text"][i])
        # adding this removes empty sentences --> if tokens:
        tokenized_sentences.append(tokens)
    return tokenized_sentences

def df_to_tokenized_2gram_sentences(df):
    tokenized_sentences = df_to_tokenized_sentences(df)
    phraser = Phrases(tokenized_sentences,min_count=1,threshold=2)
    for i in xrange(0,len(tokenized_sentences)):
        sentence = tokenized_sentences[i]
        tokenized_sentences[i] = tokens_to_bigrams(phraser,sentence)
    return tokenized_sentences
