import pandas as pd
import numpy as np
from Cleaners import clean
import json

def tsv_to_dataframe(f, hdr=0):
    return pd.read_csv(f, header=hdr, delimiter="\t", quoting=3, engine='python')

def tsv_to_df(f, hdr=0):
    return pd.read_csv(f, header=hdr, delimiter="\t", quoting=3, engine='python')

def tsvs_to_dfs(fs):
    dfs = []
    for f in fs:
        df = tsv_to_df(f)
        dfs.append(df)
    return dfs

def tsvs_to_df(fs):
    dfs = tsvs_to_dfs(fs)
    return pd.concat(dfs, ignore_index=True)

def csv_to_dataframe(f, hdr=0):
    return pd.read_csv(f, header=hdr, delimiter=",", quoting=3)

def csv_to_dataframe(f, hdr=0):
    return pd.read_csv(f, header=hdr, delimiter=",", quoting=3)

def json_to_df(file):
    with open(file) as f:
        data = json.load(f)
    return pd.DataFrame(data)

def tsv_to_fasttxt(source, target):
    tsv = pd.read_csv(source, header=0,
                      delimiter="\t", quoting=3)

    f = open(target, 'w')

    for i in range(tsv["Text"].size):
        text = tsv["Text"][i]

        f.write(text + "\t" 
                + "__label__" + str(tsv["Sentiment"][i])
                + "\n")

    f.close()

def tsv_to_cleaned_fasttxt(source, target):
    tsv = pd.read_csv(source, header=0,
                      delimiter="\t", quoting=3)

    f = open(target, 'w')

    for i in range(tsv["Text"].size):
        text = tsv["Text"][i]
        cleaned_text = clean(text)

        f.write(cleaned_text + "\t" 
                + "__label__" + str(tsv["Sentiment"][i])
                + "\n")

    f.close()

def rpw_to_df(tsv_file):
    df = tsv_to_df(tsv_file)
    dicts = []
    for i in xrange(299,999):
        id = df["ID"][i]
        text = str(df["Text"][i])
        rheza = df["Rheza"][i]
        phil = df["Phil"][i]
        wilson = df["Wilson"][i]
        if str(rheza) != 'nan' and rheza != 7 and rheza !=8:
            sentiment = int(rheza)
        elif str(phil) != 'nan' and phil != 7 and phil != 8:
            sentiment = int(phil)
        elif str(wilson) != 'nan' and phil != 7 and phil != 8:
            sentiment = int(wilson)
       
        dict = {'Text': text, 'Sentiment': sentiment, 'ID': id}
        dicts.append(dict)
    return pd.DataFrame(dicts)

def shuffle (df):
    return df.reindex(np.random.permutation(df.index))
