import scrapper.Util.mongodb as database
import sys

db = database.connect()

# Load tweets data from mongodb, sentiment = unlabelled and export to TSV
tweets = db.tweets.find({
    "sentiment" : "9"
})

with open("Result/results.tsv", "w") as record_file:
    record_file.write("ID\tText\tSentiment\n")
    for tweet in tweets:
        record_file.write("%s\t%s\t%s\n" % (tweet["id"], tweet["text_preprocessed"].replace('\n', '').strip(), tweet["sentiment"]))


# Remove the comment when facebook already implemented
# Load facebook comments from mongodb, sentiment = unlabelled, export to TSV
#facebook = db.facebook_comments.find({
#    "sentiment" : "9"
#})

#with open("Result/facebook_comments.tsv", "w") as record_file:
#    record_file.write("ID   Text\n")
#    for comment in facebook_comments:
#        record_file.write("%s   %s\n" % (comment["id"], comment["text_preprocessed"]))



