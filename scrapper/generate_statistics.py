import Util.mongodb as database
import pprint, tweepy
from datetime import datetime, timedelta
from bson.json_util import dumps
import Util.twitter as util 
import sys
import json
import dateutil.parser
"""
A dashboard that contains:

1. Most frequented users - which users mention us the most often each week. Display this as a leaderboard.
2. Most important tweets from our timelines. Determined by which tweets had the most retweets or visibility (aka seen by the most followers)
3. New followers and followers who have unfollowed us each day
4. Current emotion. Define a few categories for emotions. Define how to classify a particular tweet into that category.

There is a lot of reference material available, but I would like to start with some text mining first. At the end, this project should present us with a dashboard.


1. Most frequented users
2. Most important tweets from our timelines
3. New Followers and Followers who have unfollowed us
4. Current emotion.
"""
"""
STATS NEEDED:
- new followers
- churned followers
- positive/neutral/negative sentiment
- total tweets
- most influential tweets
- most frequent users
- list of locations of negative, neutral, positive tweets
"""

def daily_stats(day):

    day_date = str(int(day[8:])+1)
    if len(day_date)<2:
        day_date = '0'+day_date

    next = day[:8]+day_date

    result = {
        'sentiment_count' : count_sentiment(day,next), 
        'new_followers' : get_new_followers(day),
        'churned_followers' : get_churn_followers(day), 
        'num_total_followers' : get_daily_followers(day), 
        'most_frequent_users' : get_most_frequented_users(day,next), 
        'most_important_tweets' : get_most_important_tweets(day,next),
        'negative_locations' : locate_sentiment("-1", day, next),
        'positive_locations' : locate_sentiment("1", day, next)
    }

    """
    db.tweets.delete_many({"created_time":
                            {
                                '$gte' : first_day,
                                '$lte' : lsat_day
                            }
                          })

    """

    return result

def locate_sentiment(sentiment_string, first_day, last_day):
    response = db.tweets.find({"created_time" : 
                               {
                                "$gte": dateutil.parser.parse(first_day),
                                "$lte": dateutil.parser.parse(last_day)
                               }, 
                               "sentiment" : sentiment_string, 
                               "place.coordinates" : {"$exists":True}}, 
                             {"place.coordinates" : 1, "_id" : 0})
    
    result = []

    for r in response:
        coordinates = r["place"]["coordinates"][0][0]
        result.append(coordinates)

    return result

def count_sentiment(first_day, last_day):
    pipeline = [
        {'$match': {
            'created_time': {
                '$gte': first_day,
                '$lte': last_day
            }
        }},
        {"$group" :
         {"_id":
          {
              "sentiment":"$sentiment",
          }, 
          "count":{"$sum":1}}
         }    
    ]

    intermediate = db.tweets.aggregate(pipeline)
    result = []

    for i in intermediate:
        sentiment = int(i["_id"]["sentiment"])
        count = int(i["count"])

        if sentiment==-1:
            result.append(
                {
                    "sentiment" : "Negative",
                    "count" : count
                }
            )

        elif sentiment==0:
            result.append(
                {
                    "sentiment" : "Neutral",
                    "count" : count
                }
            )
        elif sentiment==1:
            result.append(
                {
                    "sentiment" : "Positive",
                    "count" : count
                }
            )
    jsdata = [{
        "data" : result
    }]

    return jsdata


def get_new_followers(day):
    result = db.followers_new.find({"date" : day})
    json_result = []
    kNum = 0
    for r in result:
        user_data = db.followers.find_one({"id" : r["id"]})
        if not user_data:
            auth = util.auth()
            api = tweepy.API(auth)
            not_inserted = True
            while not_inserted:
                try:
                    u = api.get_user(id = r["id"])
                    user_data = {
                        "id" : str(u.id),
                        "name" : u.name,
                        "screen_name" : u.screen_name,
                        "profile_image_url" : u.profile_image_url
                    }
                    db.followers.insert_one(user_data)
                    print "inserted"
                    not_inserted = False
                except Exception as e:
                    db.followers_new.delete_one({"id" : r["id"], "date" : day})
                    not_inserted = False
        if user_data:
            kNum = kNum + 1
            json = {
                "index" : kNum,
                "screen_name" : user_data["screen_name"],
                "id" : user_data["id"],
                "profile_image_url" : user_data["profile_image_url"]
            }
            json_result.append(json)
    json = {
        "data" : json_result
    }
    return json


def get_churn_followers(day):
    result = db.followers_churn.find({"date" : day})
    json_result = []
    kNum = 0
    for r in result:
        user_data = db.followers.find_one({"id" : r["id"]})
        if not user_data:
            auth = util.auth()
            api = tweepy.API(auth)
            not_inserted = True
            while not_inserted:
                try:
                    u = api.get_user(id = r["id"])
                    user_data = {
                        "id" : str(u.id),
                        "name" : u.name,
                        "screen_name" : u.screen_name,
                        "profile_image_url" : u.profile_image_url
                    }
                    db.followers.insert_one(user_data)
                    print "inserted"
                    not_inserted = False
                except Exception as e:
                    db.followers_churn.delete_one({"id" : r["id"], "date" : day})
                    not_inserted = False
        if user_data:
            kNum = kNum + 1
            json = {
                "index" : kNum,
                "screen_name" : user_data["screen_name"],
                "id" : user_data["id"],
                "profile_image_url" : user_data["profile_image_url"]
            }
            json_result.append(json)
    json = {
        "data" : json_result
    }
    return json


def get_daily_followers(day):
    result = db.followers_daily.find({"date" : day}).count()
    json = {
        "count" : result
    }
    return json


def get_most_frequented_users(first_day, last_day):
    # Most frequented users - which users mention us the most often each week. Display this as a leaderboard.
    pipeline = [
        { "$match": {
            "created_time": {
                "$gte": first_day,
                "$lt": last_day
            }
        }
        },
        {"$group" :
         {"_id":
          {
              "username":"$username",
          }, 
          "count":{"$sum":1}}
         },
        {
            "$sort":
            {"count":-1}
        },
        {
            "$limit" : 30
        }
    ]
    result = db.tweets.aggregate(pipeline)
    json_result = []
    kNum = 0
    for r in result:
        user = {}
        username = r["_id"]["username"]
        user_data = db.followers.find_one({"screen_name" : username})
        if not user_data:
            print "user data"
            print user_data
            auth = util.auth()
            api = tweepy.API(auth)
            not_inserted = True
            while not_inserted:
                try:
                    u = api.get_user(screen_name = username)
                    user_data = {
                        "id" : str(u.id),
                        "name" : u.name,
                        "screen_name" : u.screen_name,
                        "profile_image_url" : u.profile_image_url
                    }
                    db.followers.insert_one(user_data)
                    print "inserted"
                    not_inserted = False
                except Exception as e:
                    db.followers.delete_one({"screen_name" : username})
                    not_inserted = False
        if user_data:
            kNum = kNum + 1
            json = {
                "index" : kNum,
                "count" : r["count"],
                "screen_name" : username,
                "id" : user_data["id"],
                "profile_image_url" : user_data["profile_image_url"]
            }
            json_result.append(json)
    return json_result


def show_most_frequented_users(first_day, last_day):
    data = get_most_frequented_users(first_day, last_day)

    json = {
        "data" : list(data)
    }

    return json

def get_most_important_tweets(first_day, last_day):
    #Most Influential Tweets - tweets with the highest sum of favorites and retweets
    pipeline = [
        { "$match": {
            "created_time": {
                "$gte": first_day,
                "$lt": last_day
            }
        }
        },
        {"$project" : {
            'id' : '$id',
            'username' : '$username',
            'text_original' : '$text_original',
            'influence_score' : {
                "$sum" : [
                    '$retweet_count', '$favorite_count'
                ]
            },
        }
        },
        {
            "$sort" : 
            {"influence_score" : -1}
        },
        { "$limit" : 30 }
    ]
    result = db.tweets.aggregate(pipeline)

    json_data = []
    for r in result:
        json = {
            "id" : r["id"],
            "username" : r["username"].encode("latin1"),
            "influence_score" : r["influence_score"]
        }
        json_data.append(json)
    return json_data



db = database.connect()

# Get first day and last day
day = "2017-07-09"
#show_most_frequented_users(day)
day = "2017-07-16"
#show_most_frequented_users(day)
day = "2017-07-17"
#show_most_frequented_users(day)

