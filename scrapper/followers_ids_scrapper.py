import tweepy, time, yaml, datetime, sys, unicodedata, numpy
import Util.twitter as util 
import Util.mongodb as database
import timeit

auth = util.auth()
api = tweepy.API(auth)
db = database.connect()
followers = db.followers
daily_followers = db.followers_daily
new_followers = db.followers_new
churn_followers = db.followers_churn
today_arr = []
yesterday_arr = []

arr_user_id = []
next_cursor = -1

def save_new_churn_followers_to_db(db):
    # Get today's followers
    today_followers = daily_followers.find({
        "date" : str(datetime.date.today())
    }, {
        "id" : 1
    })
    
    # Get yesterday's followers
    yesterday_followers = daily_followers.find({
        "date" : str(datetime.date.today() - datetime.timedelta(days=1))
    }, {
        "id" : 1
    })
    
    for t in today_followers:
        today_arr.append(t["id"]) 
    for t in yesterday_followers:
        yesterday_arr.append(t["id"])

    #Define new and churn followers
    new_fol = numpy.setdiff1d(today_arr, yesterday_arr)
    churn_fol = numpy.setdiff1d(yesterday_arr, today_arr)
    
    # Insert new and churn followers to db
    print "number of new_fol: " + str(len(new_fol))
    print "churn_fol: " + str(len(churn_fol))

    for id in new_fol:
        fol = {
            "id" : id,
            "date" : str(datetime.date.today())
        }
        rowcount = db.followers.find({
            "id" : id
        }).count()
        if rowcount == 0:
            print "new fol id: " + str(db.followers_new.insert_one(fol).inserted_id)


    for id in churn_fol:
        fol = {
            "id" : id,
            "date" : str(datetime.date.today())
        }
        rowcount = db.followers_churn.find({
            "id" : id
        }).count()
        if rowcount == 0:
            print "churn fol id : " + str(db.followers_churn.insert_one(fol).inserted_id)


def save_user_object(api):
    daily_followers_data = daily_followers.find({"date" : str(datetime.date.today())}, {"id" : 1, "_id" : 0})
    daily_followers_list = []
    for dfollowers in daily_followers_data:
        daily_followers_list.append(str(dfollowers['id']))

    followers_data = followers.find({},{"id" : 1, "_id" : 0})
    followers_list = []
    for f in followers_data:
        followers_list.append(str(f['id']))

    # Find user that has not been recorded in followers collection
    new_data = list(set(daily_followers_list) - set(followers_list))
    print "new data " + str(len(new_data))

    # Get user object from api
    for arr in new_data:
        temp = False
        while temp == False:
            try:
                u = api.get_user(user_id = str(arr))
                followers.insert_one({
                    "id" : str(u.id),
                    "name" : u.name,
                    "screen_name" : u.screen_name,
                    "profile_image_url" : u.profile_image_url
                })
                temp = True
            except Exception as e:
                error = e[0][0]
                if "code" in error:
                    if error["code"] == "88":
                        auth = util.auth()
                        api = tweepy.API(auth)

        
def save_to_daily_followers(users):
    # Get current daily followers
    daily_followers_data = daily_followers.find({"date" : str(datetime.date.today())}, {"id" : 1, "_id" : 0})
    daily_followers_list = []
    for dfollowers in daily_followers_data:
        daily_followers_list.append(str(dfollowers['id']))
    
    # Users json 
    ids = []
    for user_id in users:
        ids.append(str(user_id))

    # Find user that has not been recorded in followers collection
    new_data = []
    new_data = list(set(ids) - set(daily_followers_list))
    print len(new_data)

    for user_id in new_data:
        daily_followers.insert_one({
            "id" : str(user_id),
            "date" : str(datetime.date.today())
        })

users = []
start = timeit.default_timer()



k = 1
for page in util.limit_handled(tweepy.Cursor(api.followers_ids, screen_name = "gojekindonesia", count=5000, cursor = next_cursor).pages(), "followers_ids", next_cursor, auth, api):
    print "Page : " + str(k)
    print len(page)
    k = k + 1
    users = page
    save_to_daily_followers(users)



"""
# User Object
k = 1
for page in util.limit_handled(tweepy.Cursor(api.followers, screen_name = "gojekindonesia", count=200, cursor = next_cursor).pages(), "followers", next_cursor, auth, api):
    print "Page : " + str(k)
    print len(page)
    for user in page:
        fol = {
            "id" : str(user.id),
            "name" : user.name,
            "screen_name" : user.screen_name,
            "profile_image_url" : user.profile_image_url
        }

        rowcount = db.followers.find({
            "id" : str(user.id)
        }).count()
        if rowcount == 0:
            db.followers.insert_one(fol)
            #print "new fol id: " + str(db.followers.insert_one(fol).inserted_id)
    k = k + 1

"""

stop = timeit.default_timer()
print "Get daily followers " + str(stop - start)

#save_user_object(api)
stop = timeit.default_timer()
print "Save user object " + str(stop - start)

save_new_churn_followers_to_db(db)
stop = timeit.default_timer()
print "New/churn followers " + str(stop - start)

