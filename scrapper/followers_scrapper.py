import tweepy, time, yaml, datetime, sys, unicodedata, numpy
import Util.twitter as util 
import Util.mongodb as database
import timeit

auth = util.auth()
api = tweepy.API(auth)
db = database.connect()
followers = db.followers
daily_followers = db.followers_daily
new_followers = db.followers_new
churn_followers = db.followers_churn
today_arr = []
yesterday_arr = []

def limit_handled(cursor, db):
    while True:
        try:
            yield cursor.next()
        except tweepy.TweepError as e:
            #save_new_churn_followers_to_db(db)
            print "Sleep : " + str(e.message)
            time.sleep(15 * 60)


def save_new_churn_followers_to_db(db):
    # Get today's followers
    today_followers = daily_followers.find({
        "date" : str(datetime.date.today())
    }, {
        "id" : 1
    })
    
    # Get yesterday's followers
    yesterday_followers = daily_followers.find({
        "date" : str(datetime.date.today() - datetime.timedelta(days=1))
    }, {
        "id" : 1
    })
    
    for t in today_followers:
        today_arr.append(t["id"]) 
    for t in yesterday_followers:
        yesterday_arr.append(t["id"])

    #Define new and churn followers
    new_fol = numpy.setdiff1d(today_arr, yesterday_arr)
    churn_fol = numpy.setdiff1d(yesterday_arr, today_arr)
    
    # Insert new and churn followers to db
    print "number of new_fol: " + str(len(new_fol))
    print "churn_fol: " + str(len(churn_fol))

    for id in new_fol:
        fol = {
            "id" : id,
            "date" : str(datetime.date.today())
        }
        rowcount = db.followers.find({
            "id" : id
        }).count()
        if rowcount == 0:
            print "new fol id: " + str(db.followers_new.insert_one(fol).inserted_id)


    for id in churn_fol:
        fol = {
            "id" : id,
            "date" : str(datetime.date.today())
        }
        rowcount = db.followers_churn.find({
            "id" : id
        }).count()
        if rowcount == 0:
            print "churn fol id : " + str(db.followers_churn.insert_one(fol).inserted_id)



def save_followers_info(users):
    for user in users:
        print "Get followers from Twitter API"
        user_id = user.id
        name = unicodedata.normalize("NFKD", user.name).encode("ascii", "ignore")
        screen_name = unicodedata.normalize("NFKD", user.screen_name).encode("ascii", "ignore")
        profile_image_url = unicodedata.normalize("NFKD", user.profile_image_url).encode("ascii", "ignore")
        
        follower = {
            "id" : user_id,
            "name" : name,
            "screen_name" : screen_name,
            "profile_image_url" : profile_image_url
        }
         
        rowcount = followers.find({
            "id" : user_id
        }).count()

        if rowcount == 0:
            # Insert followers
            print "obj id: " + str(followers.insert_one(follower).inserted_id)
            print followers.find().count()
        
        rowcount = daily_followers.find({
            "id" : user_id,
            "date" : str(datetime.date.today())
        }).count()

        if rowcount == 0:
            # Record today's followers
            print "daily obj id: " + str(daily_followers.insert_one({
                "id" : user_id,
                "date" : str(datetime.date.today())
            }).inserted_id)
            print daily_followers.find().count()
        

users = []
start = timeit.default_timer()
for page in util.limit_handled(tweepy.Cursor(api.followers, screen_name = "gojekindonesia", count=200).pages(180)):
    users = page
    print len(users)
    save_followers_info(users)
    time.sleep(30)

stop = timeit.default_timer()
print stop - start

