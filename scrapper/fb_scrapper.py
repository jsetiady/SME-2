import urllib3, facebook, os, requests, json, csv, sys, datetime, re
import Util.mongodb as database
from time import gmtime, strftime
from string import maketrans

# You might have to update the token
token= str(os.environ.get("FB_ACCESS_TOKEN"))
gojek_id = "166677693366218"
base_url = "https://graph.facebook.com/v2.7/"
graph = facebook.GraphAPI(access_token=token, version = 2.7)

comment_message = []
posts_next_page = base_url + gojek_id + "/posts?limit=100&access_token="+token
    
db = database.connect()

def get_length(posts_json):
    if not posts_json.has_key('data'):
        print "Token expired"
        return 0
    else:
        return len(posts_json['data'])

def preprocessing(text):
    t = text
    t = re.sub(r"\\u(.){4}", " ", t)                # remove unicode character
    t = re.sub(r"http(.)*?(\s|$)", "", t)           # remove url / link
    t = re.sub(r"RT\s@([A-Za-z0-9_]+):", "", t)     # remove RT + username
    t = re.sub(r"RT", "", t)                        # remove RT
    t = re.sub(r"@([A-Za-z0-9_]+)", "", t)          # remove username
    t = re.sub(r"\&(.)*;", "", t)                   # remove username
    t = re.sub(r"\^", " ", t)                       # remove username
    t = re.sub(r"(^|\s)*[0-9]+(\s|$)", " ", t)      # remove number
    t.strip()                                       #trimming
    t = re.sub(r"^[^\s]*?:", "", t)                 # remove reply
    t = re.sub(r"[^A-Za-z0-9\s\-:\#\!\?\"\'\$\%\(\)\,\.\\\/]", "", t)   # remove symbol
    t = re.sub(r"\\n", " ", t)                      # remove newline
    t = re.sub(r"\\t", " ", t)                      # remove tabs
    t = re.sub(r"\s\s+", " ", t)                    # remove multi-space
    t.strip()                                       #trimming

    # Leet to Bahasa / English
    intab = "013457"
    outtab = "oieast"
    transtab = maketrans(intab, outtab)

    str(t).translate(transtab)
    return t

k = 0
while posts_next_page != "" and k != 10:
    try:
        posts = requests.get(posts_next_page) 
        posts_json = posts.json()
        print "posts"

        if not posts_json.has_key('data'):
            print "Token expired"
            break # break from while

        # Get post id
        for x in range(0, get_length(posts_json)):
            post_id = posts_json['data'][x]['id']
            print "Post id " + str(post_id)
            comments_next_page = base_url + post_id + "/comments?limit=100&access_token="+token

            while comments_next_page != "":
                comments = requests.get(comments_next_page)
                print "Get comments for post-" + str(x)
                comments_json = comments.json()
                if comments_json.has_key('data'):
                    print "Total comment: " + str(len(comments_json['data']))
                    for y in range(0, len(comments_json['data'])):
                        data = {
                            "id" : comments_json['data'][y]['id'].encode('utf-8') ,
                            "text_original" : comments_json['data'][y]['message'].encode('utf-8') ,
                            "text_preprocessed" : preprocessing(comments_json['data'][y]['message'].encode('utf-8')) ,
                            "created_time" : comments_json['data'][y]['created_time'].encode('utf-8') ,
                            "sentiment" : "9"
                        }
                        #add mongo
                        rowcount = db.facebook_comments.find({"id": data["id"]}).count()
                        if rowcount == 0 :
                            print "obj_id: " + str(db.facebook_comments.insert_one(data).inserted_id)

                if comments_json.has_key('paging') and comments_json['paging'].has_key('next'):
                    comments_next_page = comments_json['paging']['next']
                    comments_next_page = base_url + comments_next_page[32:]
                else:
                    comments_next_page = ""
    
    except Exception as e:
        "Error " + str(e)

    if posts_json['paging'] != "" and posts_json['paging']['next'] != "":
        posts_next_page = posts_json['paging']['next']
        posts_next_page = base_url + posts_next_page[32:]
    else:
        posts_next_page = ""
    k += 1

"""
# Get comment from facebook feed
feeds_next_page = base_url + gojek_id + "/feed?limit=100&access_token="+token
while feeds_next_page != "":
    try:
        feeds = requests.get(feeds_next_page)
        for x in range(0, len(feeds_json['data'])):

            print "feeds " + str(x)
            feeds_json = feeds.json()
            if feeds_json['data'][x].has_key('message'):
                data = {
                    "id" : feeds_json['data'][x]['id'].encode('utf-8'),
                    "text_original" : feeds_json['data'][x]['text_original'].encode('utf-8'),
                    "text_preprocesing" : preprocessing(feeds_json['data'][x]['text_original'].encode('utf-8')),
                    "created_time" : feeds_json['data'][x]['created_time'].encode('utf-8'),
                    "sentiment" : "9"
                }
                rowcount = db.facebook_comments.find({"id": data["id"]}).count()
                if rowcount == 0 :
                    print "obj_id: " + str(db.facebook_comments.insert_one(data).inserted_id)

        if feeds_json.has_key('paging') and feeds_json['paging'].has_key('next'):
            feeds_next_page = feeds_json['paging']['next']
            feeds_next_page = base_url + feeds_next_page[32:]
        else:
            feeds_next_page = ""
    except:
        feeds_json = feeds.json()
"""
