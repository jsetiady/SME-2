import re, tweepy, json, csv, sys, time, datetime
from string import maketrans
import scrapper.Util.twitter as util
import scrapper.Util.mongodb as database
from time import gmtime, strftime
import json, timeit

auth = util.auth()
api = tweepy.API(auth)
db = database.connect()
tweets = db.tweets
next_cursor = -1

str_query = "gojek OR go-jek OR @gojekindonesia OR #gojek \
    OR goride OR go-ride OR gocar OR go-car  \
    OR gofood OR go-food OR #gofood OR gosend OR go-send  \
    OR gomart OR go-mart  OR goclean OR go-clean  \
    OR gomassage OR go-massage OR goglam OR go-glam  "

query = ["gojek", "go-jek", "#gojek", \
        "goride", "go-ride", "#goride", \
        "gocar", "go-car", "#gocar", \
        "gofood", "go-food", "#gofood", \
        "gosend", "go-send", "#gosend", \
        "gomart", "go-mart", "#gomart", \
        "goclean", "go-clean", "#goclean", \
        "gomassage", "go-massage", "#gomassage", \
        "goglam", "go-glam", "#goglam", \
        ]

#print "Start"
start = timeit.default_timer()
inserted_row = 0
updated_row = 0
for page in util.limit_handled(tweepy.Cursor(api.search, q=str_query, count=100).pages(180), "tweets", next_cursor, auth, api):
    for tweet in page:
        t = tweet._json
         # Get tweet properties
        tweet_id = t["id_str"]
        favorite_count = t["favorite_count"]
        followers_count = t["user"]["followers_count"]
        retweet_count = t["retweet_count"]
        
        rowcount = tweets.find({
            "id": tweet_id
        }).count()
        curtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Get location
        place = t["place"]
        if place:
            place_json = {
                "city" : place["name"],
                "country" : place["country"],
                "country_code" : place["country_code"],
                "coordinates" : place["bounding_box"]["coordinates"]
            }
        else:
            place_json = ""

        if rowcount == 0:
            if not t.has_key("retweeted_status"):
                username = t["user"]["screen_name"]

                tweet_text = t["text"]
                created_time = re.sub(r"(\+|\-)(.){4}", "", t["created_at"])
                created_time = datetime.datetime.strptime(created_time, "%a %b %d %H:%M:%S  %Y")

                # Cleaning
                t = t["text"]
                t = re.sub(r"\\u(.){4}", " ", t)                # remove unicode character
                t = re.sub(r"http(.)*?(\s|$)", "", t)           # remove url / link
                t = re.sub(r"RT\s@([A-Za-z0-9_]+):", "", t)     # remove RT + username
                t = re.sub(r"RT", "", t)                        # remove RT
                t = re.sub(r"@([A-Za-z0-9_]+)", "", t)          # remove username
                t = re.sub(r"\&(.)*;", "", t)                   # remove username
                t = re.sub(r"\^", " ", t)                       # remove username
                t = re.sub(r"(^|\s)*[0-9]+(\s|$)", " ", t)      # remove number
                t.strip()                                       #trimming
                t = re.sub(r"^[^\s]*?:", "", t)                 # remove reply
                t = re.sub(r"[^A-Za-z0-9\s\-:\#\!\?\"\'\$\%\(\)\,\.\\\/]", "", t)   # remove symbol
                t = re.sub(r"\\n", " ", t)                      # remove newline
                t = re.sub(r"\\t", " ", t)                      # remove tabs
                t = re.sub(r"\s\s+", " ", t)                    # remove multi-space
                t.strip()                                       #trimming

                # Leet to Bahasa / English
                intab = "013457"
                outtab = "oieast"
                transtab = maketrans(intab, outtab)

                str(t).translate(transtab)

                # Check whether tweet mentioned specific word
                if any(word in tweet_text for word in query):
                    #print "insert new tweet"
                    tweet = {
                        "id" : tweet_id,
                        "created_time" : created_time,
                        "scrapped_at" : curtime, 
                        "last_update" : curtime,
                        "text_original" : tweet_text,
                        "text_preprocessed" : t,
                        "username" : username,
                        "favorite_count" : str(favorite_count),
                        "retweet_count" : str(retweet_count),
                        "followers_count" : str(followers_count),
                        "place" : place_json,
                        "sentiment" : "9"
                    }
                    tweets.insert_one(tweet)
                    inserted_row = inserted_row + 1
                    #print "obj id : " + str(tweets.insert_one(tweet).inserted_id)
                    #print "count after insert: " + str(tweets.find().count())
                else:
                    #print "Skipped"
                    continue # Tweet text does not mention any words in query
            
            else:
                #print "Skipped"
                # Tweet is a retweet
                continue

        else: # Rowcount != 0, tweet exist in db
            #print "update tweet id " + tweet_id
            # Update 
            result = tweets.update_one(
                { "id" : tweet_id},
                { "$set" : {
                    "favorite_count" : str(favorite_count),
                    "retweet_count" : str(retweet_count),
                    "followers_count" : str(followers_count)
                }}, upsert = False
            )
            updated_row = updated_row + 1
            #print "Update " + str(result.matched_count) + " row"

print "Inserted : " + str(inserted_row)
print "Updated : " + str(updated_row)
stop = timeit.default_timer()
print stop - start


