import scrapper.Util.mongodb as database
import csv
import pandas

def tsv_to_dataframe(f, hdr = 0):
    return pandas.read_csv(f, header=hdr, delimiter ="\t", quoting=3)

db = database.connect()

# Tweets
# Update sentiment to DB


def update(source):
    data = tsv_to_dataframe("Result/sentiment_result.tsv")
    if source == "Twitter":
        table = db.tweets
    elif source == "Facebook":
        table = db.facebook_comments
    else:
        table = db.play_store_reviews

    for i in range(0, data.ID.count()):
        print data.at[i, 'ID']
        result = table.update_one(
            { "id" : str(data.at[i,'ID'])},
            { "$set" : {
                "sentiment" : str(data.at[i,'Sentiment']),
                "speechType" : str(data.at[i,'Speech_Type'])
            }}, upsert = False
        )
        print result.matched_count

