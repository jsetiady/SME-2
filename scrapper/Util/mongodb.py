import pymongo, pprint, os

def connect():
    # Remote
    # client = pymongo.MongoClient(os.environ.get("MONGODB_URI"))
    # db = client.heroku_xd9vcz7c
    
    # Local
    client = pymongo.MongoClient()
    db = client.SME
    
    return db


